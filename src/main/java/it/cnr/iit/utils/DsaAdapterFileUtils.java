package it.cnr.iit.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FileUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import it.cnr.iit.common.eventhandler.events.Event;
import it.cnr.iit.common.eventhandler.events.bundlemanager.BundleManagerReadResponseEvent;
import it.cnr.iit.common.eventhandler.events.multiresourcehandler.MultiResourceHandlerEvent;

@Component
public class DsaAdapterFileUtils {

	private static final Logger log = Logger.getLogger(DsaAdapterFileUtils.class.getName());

//	@Autowired
//	private VaultWrapper vault;

	public File exctractBundleReadResponseContent(BundleManagerReadResponseEvent event) {
		File f = new File(event.getContentUri());
		File res = null;
		try {
			res = File.createTempFile("contentFile", ".tmp", new File("/tmp"));
			FileUtils.copyFile(f, res);
			log.severe("returning from exctractBundleReadResponseContent res = "
					+ new String(Files.readAllBytes(Paths.get(res.getPath()))));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		f.delete();

		return res;
	}

	public File[] extractBundleReadResponse(BundleManagerReadResponseEvent event, String datalakeUri) throws Exception {
		// bundleContent[0] = policyFile
		// bundleContent[1] = contentFile
		// bundleContent[2] = headerFile
		File[] bundleContent = new File[3];

		File f = new File(event.getPolicyUri());
		bundleContent[0] = File.createTempFile("policyFile", ".fromzip", new File(new URI(datalakeUri)));
		FileUtils.copyFile(f, bundleContent[0]);
		f.delete();

//		log.severe("policy = " + new String(Files.readAllBytes(Paths.get(bundleContent[0].getPath()))));

		f = new File(event.getContentUri());
		bundleContent[1] = File.createTempFile("contentFile", ".fromzip", new File(new URI(datalakeUri)));
		FileUtils.copyFile(f, bundleContent[1]);
		f.delete();
//		decodeContent(bundleContent[1], event.getEventType());

//		log.severe("content = " + new String(Files.readAllBytes(Paths.get(bundleContent[1].getPath()))));

		f = new File(event.getMetadataUri());
		bundleContent[2] = File.createTempFile("headerFile", ".fromzip", new File(new URI(datalakeUri)));
		FileUtils.copyFile(f, bundleContent[2]);
		f.delete();

//		log.severe("header = " + new String(Files.readAllBytes(Paths.get(bundleContent[2].getPath()))));

		return bundleContent;
	}

	private void decodeContent(File file, String eventType) throws Exception {
		byte[] decodedContent = null;
		if (eventType.contains("mail")) {

		} else {
			decodedContent = Base64.getDecoder().decode((Files.readAllBytes(Paths.get(file.getPath()))));
		}
		Files.write(Paths.get(file.getPath()), decodedContent);
	}

	public File[] extractFileInZiptoFileArray(MultiResourceHandlerEvent event) throws IOException {
		File zipFile = getTmpFileFromEvent("testzip1", ".zip", event);
		File policyFile = getTmpFile("policyFile", ".fromzip");
		File contentFile = getTmpFile("contentFile", ".fromzip");
		File headerFile = getTmpFile("headerFile", ".fromzip");
		handleZip(zipFile, policyFile, contentFile, headerFile);
		return new File[] { policyFile, contentFile, headerFile };
	}

	public File[] extractFileInZiptoFileArray(Event event, String datalakeUri) throws URISyntaxException, IOException {
		// crea nuovo file tmp e ci mette il fileContent dell'evento decodato da base64
		File zipFile = getTmpFileFromEvent("testzip1", ".zip", event);

		File policyFile = getTmpFileFromUri("policyFile", ".fromzip", datalakeUri);
		File contentFile = getTmpFileFromUri("contentFile", ".fromzip", datalakeUri);
		File headerFile = getTmpFileFromUri("headerFile", ".fromzip", datalakeUri);
		handleZip(zipFile, policyFile, contentFile, headerFile);
		return new File[] { policyFile, contentFile, headerFile };
	}

	private void handleZip(File zipFile, File policyFile, File contentFile, File headerFile) throws IOException {
		try (ZipFile zip = new ZipFile(zipFile)) {
			List<ZipEntry> entries = Collections.list(zip.entries()).stream().collect(Collectors.toList());
			for (ZipEntry entry : entries) {
				String name = entry.getName();
				InputStream is = zip.getInputStream(entry);
				if (name.contains(".payload")) { // TODO endswith ?
					FileUtils.copyInputStreamToFile(is, contentFile);
				} else if (name.contains(".dsa")) {
					FileUtils.copyInputStreamToFile(is, policyFile);
				} else if (name.contains(".head")) {
					FileUtils.copyInputStreamToFile(is, headerFile);
				}
			}
		}
	}

	public FileSystemResource createFileSystemResourceFromString(String content) {
		FileSystemResource res = null;
		try {
			File file = getTmpFile("tmpFile-returnfile", ".tmp");
			FileUtils.writeStringToFile(file, content, StandardCharsets.UTF_8);
			res = new FileSystemResource(file);
		} catch (IOException e) {
			log.severe("IOException creating a new FileSystemResource : " + e.getMessage());
		}
		return res;
	}

	public File multipartFileToFile(String name, String suffix, MultipartFile multipartFile) throws IOException {
		File file = getTmpFile(name, suffix);
		FileUtils.copyInputStreamToFile(multipartFile.getInputStream(), file);
		return file;
	}

	private File getTmpFileFromEvent(String name, String suffix, Event event) throws IOException {
		File file = getTmpFile(name, suffix);
		byte[] bytes = event.getProperty("fileContent").getBytes(StandardCharsets.UTF_8);
		byte[] decodedBytes = Base64.getDecoder().decode(bytes);
		FileUtils.writeByteArrayToFile(file, decodedBytes);
		return file;
	}

	public File getTmpFileFromBytes(String name, String suffix, byte[] bytes) throws IOException {
		File file = getTmpFile(name, suffix);
		FileUtils.writeByteArrayToFile(file, bytes);
		return file;
	}

	public File getTmpFileFromUri(String name, String suffix, String uri) throws URISyntaxException, IOException {
		File file;
		try {
			file = new File(new URI(uri));
			return File.createTempFile(name, suffix, file);
		} catch (URISyntaxException e) {
			log.severe("URISyntaxException, URI was: " + uri);
			throw e;
		}
	}

	public File getTmpFile(String name, String suffix) throws IOException {
		try {
			File file = File.createTempFile(name, suffix);
			file.deleteOnExit();
			FileUtils.forceDeleteOnExit(file);
			return file;
		} catch (IOException e) {
			log.severe("IOException creating tempfile : " + e.getMessage());
			throw e;
		}
	}

	public Document convertStringToXMLDocument(String xmlString) {
		// Parser that produces DOM object trees from XML content
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		// API to obtain DOM Document instance
		DocumentBuilder builder = null;
		try {
			// Create DocumentBuilder with default configuration
			builder = factory.newDocumentBuilder();

			// Parse the content to Document object
			Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
			return doc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
