package it.cnr.iit.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.common.attributes.AttributeIds;
import it.cnr.iit.common.types.Metadata;
import it.cnr.iit.dsa_adapter.impl.DsaAdapterException;
import it.cnr.iit.xacml.attribute.AdditionalAttribute;
import it.cnr.iit.xacml.attribute.CATEGORY;
import it.cnr.iit.xacml.request_container.RequestAttributes;
import it.cnr.iit.xacml.request_container.RequestContainer;

@Component
public class DsaAdapterAttributeUtils {

	private static final Logger log = Logger.getLogger(DsaAdapterAttributeUtils.class.getName());

	public AdditionalAttribute extractAttribute(String metadata, CATEGORY c, String attributeId) {
		AdditionalAttribute attribute = null;
		try {
			RequestContainer request = new ObjectMapper().readValue(metadata.trim(), RequestContainer.class);
			for (RequestAttributes requestAttributes : request.getRequest().getAttributes()) {
				if (!requestAttributes.getAttributeId().contentEquals(attributeId)) {
					continue;
				}
				attribute = new AdditionalAttribute(c, attributeId, requestAttributes.getValue(),
						requestAttributes.getDatatype());
			}
		} catch (Exception e) {
			log.severe(() -> e.getClass().getSimpleName() + " extracting attributeId : " + attributeId + " from: "
					+ metadata);
		}
		if (attribute != null) {
			log.severe("\nextractAttribute returning " + attribute + ", value: " + attribute.getValue() + "\n");
		}

		return attribute;
	}

	public Map<String, String> getMetadataMap(String metadata, String headerContent)
			throws JsonMappingException, JsonProcessingException {

		log.severe("metadata in getMetadataMap: " + new ObjectMapper().writeValueAsString(metadata));
		RequestContainer rc = new ObjectMapper().readValue(metadata, RequestContainer.class);

		log.severe("rc.getRequest().getAttributes(): "
				+ new ObjectMapper().writeValueAsString(rc.getRequest().getAttributes()));
		Map<String, String> metadataMap = new HashMap<>();

		for (RequestAttributes ra : rc.getRequest().getAttributes()) {
			metadataMap.put(ra.getAttributeId(), ra.getValue());
			log.severe("ra row in getMetadataMap: " + new ObjectMapper().writeValueAsString(ra));
		}

//		rc.getRequest().getAttributes().stream()
//				.peek(row -> log.severe("rc.attributes: " + row.getAttributeId() + ", " + row.getValue()))
//				.map(row -> metadataMap.put(row.getAttributeId(), row.getValue()));

		metadataMap.entrySet().stream()
				.forEach(row -> log.severe("metadataMap row: " + row.getKey() + ", " + row.getValue() + "\n"));

		Map<String, String> headerMap = new HashMap<>();
		headerMap = new ObjectMapper().readValue(headerContent, new TypeReference<Map<String, String>>() {
		});

		headerMap.entrySet().stream()
				.forEach(row -> log.severe("headerMap row: " + row.getKey() + ", " + row.getValue() + "\n"));

		Map<String, String> finalMetadata = new HashMap<String, String>();

		boolean isEqual = false;
		for (Entry hrow : headerMap.entrySet()) {
			for (Entry mrow : metadataMap.entrySet()) {
				if (hrow.getKey().equals(mrow.getKey())) {
					isEqual = true;
					continue;
				}
			}
			if (!isEqual) {
				finalMetadata.put((String) hrow.getKey(), (String) hrow.getValue());
			}
			isEqual = false;
		}

		finalMetadata.entrySet().stream()
				.forEach(row -> log.severe("finalMetadata row: " + row.getKey() + ", " + row.getValue() + "\n"));
		return finalMetadata;
	}

	public RequestAttributes findRequestAttribute(List<RequestAttributes> attrList, String attributeId) {
		try {
			return attrList.stream().filter(el -> el.getAttributeId().equals(attributeId)).findFirst().orElseThrow(
					() -> new DsaAdapterException("the attribute " + attributeId + " is missing from metadata"));
		} catch (DsaAdapterException e) {
			log.severe(e.getMessage());
			return null;
		}
	}

	public AdditionalAttribute findAdditionalAttribute(List<AdditionalAttribute> attrList, String attributeId) {
		try {
			return attrList.stream().filter(el -> el.getName().equals(attributeId)).findFirst().orElseThrow(
					() -> new DsaAdapterException("the attribute " + attributeId + "is missing from metadata"));
		} catch (DsaAdapterException e) {
			log.severe(e.getMessage());
			return null;
		}
	}

	public List<RequestAttributes> fromAdditionalAttributeToRequestAttributes(List<AdditionalAttribute> list) {
		List<RequestAttributes> resultList = new ArrayList<RequestAttributes>();
		for (AdditionalAttribute attr : list) {
			resultList.add(new RequestAttributes(attr.getName(), attr.getValue(), attr.getDataType()));
		}
		return resultList;
	}

	public List<AdditionalAttribute> fromRequestAttributesToAdditionalAttribute(List<RequestAttributes> list) {
		List<AdditionalAttribute> resultList = new ArrayList<AdditionalAttribute>();

		for (RequestAttributes attr : list) {
			if (attr.getAttributeId().contains(":subject:")) {
				resultList.add(new AdditionalAttribute(CATEGORY.SUBJECT, attr.getAttributeId(), attr.getValue(),
						attr.getDatatype()));
			} else if (attr.getAttributeId().contains(":action:")) {
				resultList.add(new AdditionalAttribute(CATEGORY.ACTION, attr.getAttributeId(), attr.getValue(),
						attr.getDatatype()));
			} else if (attr.getAttributeId().contains(":resource:")) {
				resultList.add(new AdditionalAttribute(CATEGORY.RESOURCE, attr.getAttributeId(), attr.getValue(),
						attr.getDatatype()));
			}
		}
		return resultList;
	}

	public Vector<AdditionalAttribute> generateAttrVect(List<RequestAttributes> attrList, String action) {
		Vector<AdditionalAttribute> attrVect = new Vector<AdditionalAttribute>();

		addAttributeToVector(attrList, AttributeIds.AGGREGATION_OPERATION, CATEGORY.ACTION, "string", attrVect);
		addAttributeToVector(attrList, AttributeIds.IAI_SESSION_ID, CATEGORY.ACTION, "string", attrVect);
		attrVect.add(new AdditionalAttribute(CATEGORY.ACTION, AttributeIds.ACTION_ID, action, "string"));
		addAttributeToVector(attrList, AttributeIds.SUBJECT_ID, CATEGORY.SUBJECT, "string", attrVect);
		addAttributeToVector(attrList, AttributeIds.AUTHENTICATION_TYPE, CATEGORY.SUBJECT, "string", attrVect);
		addAttributeToVector(attrList, AttributeIds.SUBJECT_ORGANIZATION, CATEGORY.SUBJECT, "string", attrVect);
		addAttributeToVector(attrList, AttributeIds.ACCESS_PURPOSE, CATEGORY.SUBJECT, "string", attrVect);
		addAttributeToVector(attrList, AttributeIds.RESOURCE_ID, CATEGORY.RESOURCE, "string", attrVect);
		addAttributeToVector(attrList, AttributeIds.RESOURCE_OWNER, CATEGORY.RESOURCE, "string", attrVect);
		addAttributeToVector(attrList, AttributeIds.RESOURCE_TYPE, CATEGORY.RESOURCE, "string", attrVect);
		addAttributeToVector(attrList, AttributeIds.DSA_ID, CATEGORY.RESOURCE, "string", attrVect);
		addAttributeToVector(attrList, AttributeIds.START_DATE, CATEGORY.RESOURCE, "date", attrVect);
		addAttributeToVector(attrList, AttributeIds.START_TIME, CATEGORY.RESOURCE, "time", attrVect);
		addAttributeToVector(attrList, AttributeIds.END_DATE, CATEGORY.RESOURCE, "date", attrVect);
		addAttributeToVector(attrList, AttributeIds.END_TIME, CATEGORY.RESOURCE, "time", attrVect);

		return attrVect;
	}

	public void addAttributeToVector(List<RequestAttributes> attrList, String attributeId, CATEGORY category,
			String dataType, Vector<AdditionalAttribute> attrVect) {
		RequestAttributes attribute = findRequestAttribute(attrList, attributeId);
		if (attribute != null) {
			attrVect.add(new AdditionalAttribute(category, attributeId, attribute.getValue(), dataType));
		}
	}

//	public void extractHeaderMetadata(List<RequestAttributes> attrList, Metadata metadata) {
//		AdditionalAttribute[] times = parseTime(metadata.getStartTime(), metadata.getEndTime());
//		attrList.add(new RequestAttributes(AttributeIds.RESOURCE_ID, metadata.getId(), "string"));
//		attrList.add(new RequestAttributes(AttributeIds.DSA_ID, metadata.getDsaId(), "string"));
//		attrList.add(new RequestAttributes(AttributeIds.START_DATE, times[0].getValue(), "date"));
//		attrList.add(new RequestAttributes(AttributeIds.START_TIME, times[1].getValue(), "time"));
//		attrList.add(new RequestAttributes(AttributeIds.END_DATE, times[2].getValue(), "date"));
//		attrList.add(new RequestAttributes(AttributeIds.END_TIME, times[3].getValue(), "time"));
//		attrList.add(new RequestAttributes(AttributeIds.RESOURCE_OWNER, metadata.getOrganization(), "string"));
//		attrList.add(new RequestAttributes(AttributeIds.RESOURCE_TYPE, metadata.getEventType(), "string"));
//	}

	public void extractHeaderMetadata(List<AdditionalAttribute> attrList, Metadata metadata) {
		AdditionalAttribute[] times = parseTime(metadata.getStartTime(), metadata.getEndTime());
		attrList.add(new AdditionalAttribute(CATEGORY.RESOURCE, AttributeIds.RESOURCE_ID, metadata.getId(), "string"));
		attrList.add(new AdditionalAttribute(CATEGORY.RESOURCE, AttributeIds.DSA_ID, metadata.getDsaId(), "string"));
		attrList.add(new AdditionalAttribute(CATEGORY.RESOURCE, AttributeIds.START_DATE, times[0].getValue(), "date"));
		attrList.add(new AdditionalAttribute(CATEGORY.RESOURCE, AttributeIds.START_TIME, times[1].getValue(), "time"));
		attrList.add(new AdditionalAttribute(CATEGORY.RESOURCE, AttributeIds.END_DATE, times[2].getValue(), "date"));
		attrList.add(new AdditionalAttribute(CATEGORY.RESOURCE, AttributeIds.END_TIME, times[3].getValue(), "time"));
		attrList.add(new AdditionalAttribute(CATEGORY.RESOURCE, AttributeIds.RESOURCE_OWNER, metadata.getOrganization(),
				"string"));
		attrList.add(new AdditionalAttribute(CATEGORY.RESOURCE, AttributeIds.RESOURCE_TYPE, metadata.getEventType(),
				"string"));
	}

	private AdditionalAttribute[] parseTime(String raw_start, String raw_end) {
		String[] parsedTime = new String[4];
		// startDate
		parsedTime[0] = raw_start.split("T")[0];
		// startTime
		parsedTime[1] = raw_start.split("T")[1].split("\\.")[0];
		// endDate
		parsedTime[2] = raw_end.split("T")[0];
		// endTime
		parsedTime[3] = raw_end.split("T")[1].split("\\.")[0];

		AdditionalAttribute[] dateAttrs = new AdditionalAttribute[4];

		// startDate
		dateAttrs[0] = new AdditionalAttribute(CATEGORY.RESOURCE, AttributeIds.START_DATE, parsedTime[0], "date");
		// startTime
		dateAttrs[1] = new AdditionalAttribute(CATEGORY.RESOURCE, AttributeIds.START_TIME, parsedTime[1], "time");
		// endDate
		dateAttrs[2] = new AdditionalAttribute(CATEGORY.RESOURCE, AttributeIds.END_DATE, parsedTime[2], "date");
		// endTime
		dateAttrs[3] = new AdditionalAttribute(CATEGORY.RESOURCE, AttributeIds.END_TIME, parsedTime[3], "time");

		return dateAttrs;
	}

//	public String[] unmarshalTriggeredObligations(String obligations) {
//		obligations = obligations.substring(1, obligations.length() - 1);
//		String[] res = obligations.split(",");
//		for (int i = 0; i < res.length; i++) {
//			res[i] = res[i].substring(1, res[i].length() - 1);
//		}
//
//		return res;
//	}
}
