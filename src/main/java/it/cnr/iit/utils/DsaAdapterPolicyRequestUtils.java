package it.cnr.iit.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.att.research.xacml.api.Request;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.common.types.Metadata;
import it.cnr.iit.xacml.RequestGenerator;
import it.cnr.iit.xacml.RequestSerializationFactory;
import it.cnr.iit.xacml.attribute.AdditionalAttribute;

@Component
public class DsaAdapterPolicyRequestUtils {

	@Autowired
	private ComponentWrapper componentWrapper;

	private static final Logger log = Logger.getLogger(DsaAdapterPolicyRequestUtils.class.getName());

	public List<String> getRequestFiles(File dataLakeFile, List<AdditionalAttribute> aaList,
			Map<String, File[]> dpoMap) {
		List<String> list = new ArrayList<>();
		for (Entry<String, File[]> dpoEntry : dpoMap.entrySet()) {
			try {
				List<AdditionalAttribute> tmp = new ArrayList<AdditionalAttribute>();
				tmp.addAll(aaList);
				File headerFile = dpoEntry.getValue()[2];
				BufferedReader br = new BufferedReader(new FileReader(headerFile));
				String line = br.readLine();
				log.severe("line in getRequestFiles: " + line);
				br.close();

				Metadata headerMetadata = new ObjectMapper().readValue(line, Metadata.class);
				componentWrapper.getDsaAdapterAttributeUtils().extractHeaderMetadata(tmp, headerMetadata);
				AdditionalAttribute[] addAttrArray = tmp.toArray(new AdditionalAttribute[tmp.size()]);

				Request request = new RequestGenerator().createXACMLV3Request(true, addAttrArray);
				String xacmlRequest = RequestSerializationFactory.newInstance().serialize(request,
						RequestSerializationFactory.SERIALIZATION_FORMAT.XML);
				File requestFile = File.createTempFile("request-" + dpoEntry.getKey(), ".xml", dataLakeFile);
				FileUtils.writeStringToFile(requestFile, xacmlRequest, StandardCharsets.UTF_8);
				list.add(requestFile.getAbsolutePath());
			} catch (Exception e) {
				e.printStackTrace();
				log.severe("could not read : " + dpoEntry.getValue()[2].getAbsolutePath());
			}
		}
		return list;
	}

	public List<String> getPolicyFiles(File dataLakeFile, Map<String, File[]> dpoFilesMap) {
		List<String> list = new ArrayList<>();
		for (Entry<String, File[]> dpoEntry : dpoFilesMap.entrySet()) {
			try { // added getKey()
				File policyFile = File.createTempFile("policy-" + dpoEntry.getKey(), ".xml", dataLakeFile);
				FileUtils.copyFile(dpoEntry.getValue()[0], policyFile);
				list.add(policyFile.getAbsolutePath());
			} catch (IOException e) {
				log.severe("could not write policy for dpoId : " + dpoEntry);
			}
		}
		return list;
	}

}
