package it.cnr.iit.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import it.cnr.iit.common.eventhandler.events.Event;
import it.cnr.iit.common.eventhandler.events.RequestEvent;
import it.cnr.iit.common.eventhandler.events.multiresourcehandler.MultiResourceHandlerEvent;
import it.cnr.iit.common.types.DataManipulationObject;
import it.cnr.iit.common.utility.FileUtility;
import it.cnr.iit.dsa_adapter.eventhandler.EventHandlerService;
import it.cnr.iit.dsa_adapter.impl.DsaAdapterException;

@Component
public class DsaAdapterEventUtils {

	private static final Logger log = Logger.getLogger(DsaAdapterEventUtils.class.getName());

	private static final long MAX_POLL_MINUTES = 3;

	private BlockingQueue<RequestEvent> getEventQueue(EventHandlerService eventHandler, RequestEvent event)
			throws DsaAdapterException {
		try {
			return eventHandler.notify(event);
		} catch (Exception e) {
			log.severe("error in registering as listeners to Event Manager " + e.getMessage());
			throw new DsaAdapterException("error handling event");
		}
	}

	public Event sendEvent(EventHandlerService eventHandler, RequestEvent event)
			throws DsaAdapterException, InterruptedException {
		BlockingQueue<RequestEvent> queue = getEventQueue(eventHandler, event);
		RequestEvent eventResponse = null;
		try {
			eventResponse = queue.poll(MAX_POLL_MINUTES, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			log.severe("The maximum time for waiting response from EventHandler has expired");
			throw e;
		}
		if (eventResponse == null) {
			log.severe(() -> "error in receiving response from EventHandler for message: " + event.toString());
			throw new DsaAdapterException("no response received for: " + event.toString());
		}

		return eventResponse;
	}

	public List<DataManipulationObject> extractDmo(MultiResourceHandlerEvent event, String dpoId, String action,
			File content) {
		List<DataManipulationObject> res = new ArrayList<DataManipulationObject>();

		for (int count = 0; event.getAdditionalProperties().get("dmo" + count) != null; count++) {
			String[] dmos = event.getAdditionalProperties().get("dmo" + count).split(",");
			DataManipulationObject dmo = new DataManipulationObject(dmos[0], dmos[1], dmos[2]);
			dmo.addAdditionalInfo("action", action);
			if (dpoId != null) {
				dmo.addAdditionalInfo("dpoId", dpoId);
			}
			if (content != null) {
				dmo.addAdditionalInfo("content", FileUtility.readFileAsString(content));
			}
			res.add(dmo);
			event.getAdditionalProperties().remove("dmo" + count);
		}
		return res;
	}

}
