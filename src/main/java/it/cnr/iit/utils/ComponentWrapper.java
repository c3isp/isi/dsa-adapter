package it.cnr.iit.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.cnr.iit.dsa_adapter.db.SessionRecorderStorage;
import it.cnr.iit.dsa_adapter.eventhandler.EventHandlerService;
import it.cnr.iit.dsa_adapter.helpers.BufferManagerHelper;
import it.cnr.iit.dsa_adapter.helpers.BundleManagerHelper;
import it.cnr.iit.dsa_adapter.helpers.DsaManagerHelper;
import it.cnr.iit.dsa_adapter.helpers.IAIHelper;
import it.cnr.iit.dsa_adapter.helpers.MoveHelper;
import it.cnr.iit.dsa_adapter.helpers.MultiResourceHandlerHelper;
import it.cnr.iit.dsa_adapter.impl.DsaAdapterService;
import it.cnr.iit.dsa_adapter.properties.BufferManagerProperties;
import it.cnr.iit.dsa_adapter.properties.DsaAdapterProperties;
import it.cnr.iit.dsa_adapter.properties.EventHandlerProperties;

@Component
public class ComponentWrapper {

	@Autowired
	private BundleManagerHelper bundleManager;

	@Autowired
	private BufferManagerHelper bufferManager;

	@Autowired
	private MoveHelper moveHelper;

	@Autowired
	private MultiResourceHandlerHelper multiResourceHandler;

	@Autowired
	private DsaManagerHelper dsaManager;

	@Autowired
	private EventHandlerService eventHandler;

	@Autowired
	private SessionRecorderStorage sessionRecorderStorage;

	@Autowired
	private DsaAdapterFileUtils dsaAdapterFileUtils;

	@Autowired
	private DsaAdapterEventUtils dsaAdapterEventUtils;

	@Autowired
	private DsaAdapterAttributeUtils dsaAdapterAttributeUtils;

	@Autowired
	private DsaAdapterPolicyRequestUtils dsaAdapterPolicyRequestUtils;

	@Autowired
	private EventHandlerProperties eventHandlerProperties;

	@Autowired
	private DsaAdapterProperties dsaAdapterProperties;

	@Autowired
	private BufferManagerProperties bufferManagerProperties;

	@Autowired
	private DsaAdapterService dsaAdapter;

	@Autowired
	private IAIHelper iaiHelper;

	public ComponentWrapper() {
		// TODO Auto-generated constructor stub
	}

	public BundleManagerHelper getBundleManager() {
		return bundleManager;
	}

	public void setBundleManager(BundleManagerHelper bundleManager) {
		this.bundleManager = bundleManager;
	}

	public MoveHelper getMoveHelper() {
		return moveHelper;
	}

	public void setMoveHelper(MoveHelper moveHelper) {
		this.moveHelper = moveHelper;
	}

	public MultiResourceHandlerHelper getMultiResourceHandler() {
		return multiResourceHandler;
	}

	public void setMultiResourceHandler(MultiResourceHandlerHelper multiResourceHandler) {
		this.multiResourceHandler = multiResourceHandler;
	}

	public DsaManagerHelper getDsaManager() {
		return dsaManager;
	}

	public void setDsaManager(DsaManagerHelper dsaManager) {
		this.dsaManager = dsaManager;
	}

	public EventHandlerService getEventHandler() {
		return eventHandler;
	}

	public void setEventHandler(EventHandlerService eventHandler) {
		this.eventHandler = eventHandler;
	}

	public SessionRecorderStorage getSessionRecorderStorage() {
		return sessionRecorderStorage;
	}

	public void setSessionRecorderStorage(SessionRecorderStorage sessionRecorderStorage) {
		this.sessionRecorderStorage = sessionRecorderStorage;
	}

	public DsaAdapterFileUtils getDsaAdapterFileUtils() {
		return dsaAdapterFileUtils;
	}

	public void setDsaAdapterFileUtils(DsaAdapterFileUtils dsaAdapterFileUtils) {
		this.dsaAdapterFileUtils = dsaAdapterFileUtils;
	}

	public DsaAdapterEventUtils getDsaAdapterEventUtils() {
		return dsaAdapterEventUtils;
	}

	public void setDsaAdapterEventUtils(DsaAdapterEventUtils dsaAdapterEventUtils) {
		this.dsaAdapterEventUtils = dsaAdapterEventUtils;
	}

	public DsaAdapterAttributeUtils getDsaAdapterAttributeUtils() {
		return dsaAdapterAttributeUtils;
	}

	public void setDsaAdapterAttributeUtils(DsaAdapterAttributeUtils dsaAdapterAttributeUtils) {
		this.dsaAdapterAttributeUtils = dsaAdapterAttributeUtils;
	}

	public DsaAdapterPolicyRequestUtils getDsaAdapterPolicyRequestUtils() {
		return dsaAdapterPolicyRequestUtils;
	}

	public void setDsaAdapterPolicyRequestUtils(DsaAdapterPolicyRequestUtils dsaAdapterPolicyRequestUtils) {
		this.dsaAdapterPolicyRequestUtils = dsaAdapterPolicyRequestUtils;
	}

	public EventHandlerProperties getEventHandlerProperties() {
		return eventHandlerProperties;
	}

	public void setEventHandlerProperties(EventHandlerProperties eventHandlerProperties) {
		this.eventHandlerProperties = eventHandlerProperties;
	}

	public DsaAdapterProperties getDsaAdapterProperties() {
		return dsaAdapterProperties;
	}

	public void setDsaAdapterProperties(DsaAdapterProperties dsaAdapterProperties) {
		this.dsaAdapterProperties = dsaAdapterProperties;
	}

	public BufferManagerProperties getBufferManagerProperties() {
		return bufferManagerProperties;
	}

	public void setBufferManagerProperties(BufferManagerProperties bufferManagerProperties) {
		this.bufferManagerProperties = bufferManagerProperties;
	}

	public DsaAdapterService getDsaAdapter() {
		return dsaAdapter;
	}

	public void setDsaAdapter(DsaAdapterService dsaAdapter) {
		this.dsaAdapter = dsaAdapter;
	}

	public IAIHelper getIaiHelper() {
		return iaiHelper;
	}

	public void setIaiHelper(IAIHelper iaiHelper) {
		this.iaiHelper = iaiHelper;
	}

	public BufferManagerHelper getBufferManager() {
		return bufferManager;
	}

	public void setBufferManager(BufferManagerHelper bufferManager) {
		this.bufferManager = bufferManager;
	}

}
