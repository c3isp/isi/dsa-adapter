package it.cnr.iit.dsa_adapter.properties;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.util.UriComponentsBuilder;

@Configuration
public class EventHandlerProperties {

	@Value("${api.eventhandler.uri}")
	@NotNull
	private String eventHandlerUri;

	@Value("${api.eventhandler.subscribers}")
	@NotNull
	private String eventHandlerSubscribers;

	@Value("${api.eventhandler.subscribe}")
	@NotNull
	private String eventHandlerSubscribe;

	@Value("${api.eventhandler.notifyevent}")
	@NotNull
	private String eventHandlerNotifyEvent;

	public EventHandlerProperties() {

	}

	public String getEventHandlerUri() {
		return eventHandlerUri;
	}

	public void setEventHandlerUri(String eventHandlerUri) {
		this.eventHandlerUri = eventHandlerUri;
	}

	public String getEventHandlerSubscribers() {
		return eventHandlerSubscribers;
	}

	public String getEventHandlerSubscribersAPI() {
		return getApiUri(getEventHandlerUri(), getEventHandlerSubscribers());
	}

	public void setEventHandlerSubscribers(String eventHandlerSubscribers) {
		this.eventHandlerSubscribers = eventHandlerSubscribers;
	}

	public String getEventHandlerSubscribe() {
		return eventHandlerSubscribe;
	}

	public String getEventHandlerSubscribeAPI() {
		return getApiUri(getEventHandlerUri(), getEventHandlerSubscribe());
	}

	public void setEventHandlerSubscribe(String eventHandlerSubscribe) {
		this.eventHandlerSubscribe = eventHandlerSubscribe;
	}

	public String getEventHandlerNotifyEvent() {
		return eventHandlerNotifyEvent;
	}

	public String getEventHandlerNotifyEventAPI() {
		return getApiUri(getEventHandlerUri(), getEventHandlerNotifyEvent());
	}

	public void setEventHandlerNotifyEvent(String eventHandlerNotifyEvent) {
		this.eventHandlerNotifyEvent = eventHandlerNotifyEvent;
	}

	public String getApiUri(String url, String path) {
		return UriComponentsBuilder.fromUriString(url).path(path).build().toUri().toString();
	}

}
