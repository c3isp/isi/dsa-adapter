package it.cnr.iit.dsa_adapter.properties;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.util.UriComponentsBuilder;

@Configuration
public class DsaAdapterProperties {

	@Value("${api.self.uri}")
	@NotNull
	private String uri;

	@Value("${api.self.notifyevent}")
	@NotNull
	private String notifyEvent;

	public DsaAdapterProperties() {

	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getNotifyEvent() {
		return notifyEvent;
	}

	public void setNotifyEvent(String notifyEvent) {
		this.notifyEvent = notifyEvent;
	}

	public String getNotifyEventAPI() {
		return getApiUri(getUri(), getNotifyEvent());
	}

	public String getApiUri(String url, String path) {
		return UriComponentsBuilder.fromUriString(url).path(path).build().toUri().toString();
	}

}
