package it.cnr.iit.dsa_adapter.properties;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.util.UriComponentsBuilder;

import it.cnr.iit.dsa_adapter.enums.DATA_LAKE;
import it.cnr.iit.dsa_adapter.enums.DATA_TYPE;
import it.cnr.iit.dsa_adapter.enums.FORMAT;

@Configuration
public class BufferManagerProperties {

	@Value("${api.buffermanager.uri}")
	@NotNull
	private String uri;

	@Value("${api.buffermanager.preparedata}")
	@NotNull
	private String prepareData;

	@Value("${api.buffermanager.prepareemptydatalake}")
	@NotNull
	private String prepareEmptyDatalake;

	public BufferManagerProperties() {
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getPrepareData() {
		return prepareData;
	}

	public void setPrepareData(String prepareData) {
		this.prepareData = prepareData;
	}

	public String getPrepareDataAPI(String serviceName, DATA_LAKE dataLake, DATA_TYPE dataType, FORMAT format) {
		return UriComponentsBuilder.fromUriString(getUri()).path(getPrepareData()).queryParam("data_lake", dataLake)
				.queryParam("format", format).queryParam("service_name", serviceName).queryParam("type", dataType)
				.build().toUri().toString();
//		return "https://isic3isp.iit.cnr.it:8443/isi/buffer-manager/prepareData?data_lake=" + dataLake + "&format="
//				+ format + "&service_name=InvokeBruteForceAttacksDetection&type=FS";
	}

	public String getPrepareEmptyDatalake() {
		return prepareEmptyDatalake;
	}

	public void setPrepareEmptyDatalake(String prepareEmptyDatalake) {
		this.prepareEmptyDatalake = prepareEmptyDatalake;
	}

	public String getPrepareEmptyDatalakeAPI(DATA_LAKE dataLake, DATA_TYPE dataType) {
		return UriComponentsBuilder.fromUriString(getUri()).path(getPrepareEmptyDatalake())
				.queryParam("data_lake", dataLake).queryParam("type", dataType).build().toUri().toString();
	}
}
