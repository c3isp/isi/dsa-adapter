package it.cnr.iit.dsa_adapter.helpers;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.common.attributes.AttributeIds;
import it.cnr.iit.common.attributes.AttributesUtility;
import it.cnr.iit.common.types.Metadata;
import it.cnr.iit.utils.ComponentWrapper;
import it.cnr.iit.utils.DsaAdapterPolicyRequestUtils;
import it.cnr.iit.xacml.request_container.RequestAttributes;

@Component
public class DsaManagerHelper {

	private static final Logger log = Logger.getLogger(DsaAdapterPolicyRequestUtils.class.getName());

	@Value(value = "${api.dsamanager.uri}")
	private String dsaManagerUrl;
	@Value(value = "${api.dsamanager.fetchusagepolicy}")
	private String dsaFetchUsagePolicy;

	@Value(value = "${security.user.name}")
	private String restUser;
	@Value(value = "${security.user.password}")
	private String restPassword;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ComponentWrapper componentWrapper;

	public Metadata getMetadata(List<RequestAttributes> attrList) throws IOException {

		Metadata cm = new Metadata();
		cm.setDsaId(componentWrapper.getDsaAdapterAttributeUtils().findRequestAttribute(attrList, AttributeIds.DSA_ID)
				.getValue());
		String startDate = componentWrapper.getDsaAdapterAttributeUtils()
				.findRequestAttribute(attrList, AttributeIds.START_DATE).getValue();
		String startTime = componentWrapper.getDsaAdapterAttributeUtils()
				.findRequestAttribute(attrList, AttributeIds.START_TIME).getValue();
		String endDate = componentWrapper.getDsaAdapterAttributeUtils()
				.findRequestAttribute(attrList, AttributeIds.END_DATE).getValue();
		String endTime = componentWrapper.getDsaAdapterAttributeUtils()
				.findRequestAttribute(attrList, AttributeIds.END_TIME).getValue();
		cm.setStartTime(AttributesUtility.unparseTime(startDate, startTime));
		cm.setEndTime(AttributesUtility.unparseTime(endDate, endTime));
		cm.setEventType(componentWrapper.getDsaAdapterAttributeUtils()
				.findRequestAttribute(attrList, AttributeIds.RESOURCE_TYPE).getValue());
		cm.setOrganization(componentWrapper.getDsaAdapterAttributeUtils()
				.findRequestAttribute(attrList, AttributeIds.RESOURCE_OWNER).getValue());
		return cm;
	}

	public String getPolicy(String dsaId) {
		URI dsaAddress = UriComponentsBuilder.fromUriString(this.dsaManagerUrl).path(dsaFetchUsagePolicy)
				.pathSegment(new String[] { dsaId }).build().toUri();
		ResponseEntity<String> response = this.restTemplate.getForEntity(dsaAddress, String.class);
		return response.getBody();
	}

	public List<String> filterDenyResponses(File responseFile) {
		List<String> res = new ArrayList<>();

		try {
			JsonNode jsonFile = new ObjectMapper().readTree(responseFile);
			int count = 0;
			while (jsonFile.get(count) != null) {
				if (jsonFile.get(count).get("response").textValue().contains("Deny")) {
					res.add(jsonFile.get(count).get("dpoID").asText());
				}
				count++;

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return res;
	}

//	public void performObligation(String policy, String triggeredObligations, String dpoId) {
//		String[] obls = componentWrapper.getDsaAdapterAttributeUtils()
//				.unmarshalTriggeredObligations(triggeredObligations);
//		Document doc = componentWrapper.getDsaAdapterFileUtils().convertStringToXMLDocument(policy);
//		List<Node> triggeredList = new ArrayList<Node>();
//		try {
//			XPathFactory xPathfactory = XPathFactory.newInstance();
//			XPath xpath = xPathfactory.newXPath();
//			XPathExpression expr = xpath.compile("//ObligationExpression");
//			NodeList extractedElements = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
//			for (int i = 0; i < extractedElements.getLength(); i++) {
//				Node element = extractedElements.item(i);
//				Node xacmlRuleNode = element.getParentNode().getParentNode();
//				for (int j = 0; j < obls.length; j++) {
//					if (xacmlRuleNode.getAttributes().getNamedItem("RuleId").getNodeValue().compareTo(obls[j]) == 0) {
//						log.severe(
//								"target found: " + xacmlRuleNode.getAttributes().getNamedItem("RuleId").getNodeValue());
//						String val = element.getAttributes().getNamedItem("ObligationId").getNodeValue();
//						String val_decoded = URLDecoder.decode(val, StandardCharsets.UTF_8.toString()).toString();
//						String actionID = val.split("%")[0];
//						String attribute = val_decoded.split("\\{")[1].split("\\}")[0];
//						log.severe("detected actionID: " + actionID);
//						log.severe("detected attribute: " + attribute);
//						triggeredList.add(element);
//					}
//				}
//
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		for (Node n : triggeredList) {
//			ObligationEngine.processObligation(n, dpoId);
//		}
//
//	}

}
