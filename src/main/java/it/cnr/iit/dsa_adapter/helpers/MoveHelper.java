package it.cnr.iit.dsa_adapter.helpers;

import java.io.File;
import java.net.URI;
import java.util.Base64;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import it.cnr.iit.common.eventhandler.events.ResponseEvent;

@Component
public class MoveHelper {

	private static final Logger log = Logger.getLogger(MoveHelper.class.getName());

	@Value(value = "${security.user.name}")
	private String restUser;
	@Value(value = "${security.user.password}")
	private String restPassword;
	@Value(value = "${api.dsa-adapter.endpoint.receiveDpo}")
	private String path;

	@Autowired
	private RestTemplate restTemplate;

	public ResponseEntity<ResponseEvent> sendDPO(File bundleContent, String metadata, String destination) {
		String plainCreds = restUser + ":" + restPassword;
		String base64Creds = new String(Base64.getEncoder().encode(plainCreds.getBytes()));

		HttpHeaders headersTransfer = new HttpHeaders();
		headersTransfer.add("Authorization", "Basic " + base64Creds);
		LinkedMultiValueMap<String, Object> toUpload = new LinkedMultiValueMap<>();
		toUpload.add("file", new FileSystemResource(bundleContent));
		toUpload.add("input_metadata", metadata);
		RequestEntity<MultiValueMap<String, Object>> requestEntity;
		URI endpoint = UriComponentsBuilder.fromUriString(destination).path(path).build().toUri();
		log.severe("sending dpo to " + endpoint.toString());
		requestEntity = RequestEntity.post(endpoint).headers(headersTransfer).contentType(MediaType.MULTIPART_FORM_DATA)
				.body(toUpload);

		ResponseEntity<ResponseEvent> response = restTemplate.exchange(requestEntity, ResponseEvent.class);

		return response;
	}
}
