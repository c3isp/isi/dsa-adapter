package it.cnr.iit.dsa_adapter.helpers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import it.cnr.iit.common.eventhandler.events.multiresourcehandler.MultiResourceHandlerEvent;
import it.cnr.iit.common.eventhandler.events.multiresourcehandler.MultiResourceHandlerEventEndAccess;
import it.cnr.iit.common.eventhandler.events.multiresourcehandler.MultiResourceHandlerEventTryAccess;
import it.cnr.iit.common.eventhandler.events.multiresourcehandler.MultiResourceHandlerEventTryAccessMulti;
import it.cnr.iit.dsa_adapter.impl.DsaAdapterException;
import it.cnr.iit.utils.ComponentWrapper;

@Component
public class MultiResourceHandlerHelper {

	private static final Logger log = Logger.getLogger(MultiResourceHandlerHelper.class.getName());

	@Autowired
	private ComponentWrapper componentWrapper;

	@Value(value = "${security.user.name}")
	private String restUser;
	@Value(value = "${security.user.password}")
	private String restPassword;

	public Optional<MultiResourceHandlerEvent> checkDpos(String sessionId, List<String> policyFileList,
			List<String> requestFileList, boolean prepareData) {
		MultiResourceHandlerEvent event = null;
		if (prepareData) {
			event = createTryAccessMulti(policyFileList.toArray(new String[0]), requestFileList.toArray(new String[0]),
					sessionId);
		} else {
			event = createTryAccess(fromUriToXML(policyFileList.get(0)), fromUriToXML(requestFileList.get(0)));
		}

		try {
			MultiResourceHandlerEvent responseEvent = (MultiResourceHandlerEvent) componentWrapper
					.getDsaAdapterEventUtils().sendEvent(componentWrapper.getEventHandler(), event);
			return Optional.of(responseEvent);
		} catch (InterruptedException e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
			Thread.currentThread().interrupt();
		} catch (DsaAdapterException e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
		}

		return Optional.empty();
	}

	public static MultiResourceHandlerEvent createTryAccessMulti(String[] xacmlPolicies, String[] xacmlRequests,
			String sessionId) {
		return new MultiResourceHandlerEventTryAccessMulti(xacmlPolicies, xacmlRequests, sessionId);
	}

	public static MultiResourceHandlerEvent createTryAccess(String xacmlPolicy, String xacmlRequest) {
		return new MultiResourceHandlerEventTryAccess(xacmlPolicy, xacmlRequest);
	}

	public static String fromUriToXML(String uri) {
		if (uri.startsWith("<?xml")) {
			return uri;
		}
		String res = null;
		try {
			byte[] encoded;
			encoded = Files.readAllBytes(Paths.get(uri));
			res = new String(encoded, StandardCharsets.UTF_8);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}

	private MultiResourceHandlerEvent createEndAccess(String sessionId) {
		return new MultiResourceHandlerEventEndAccess(sessionId);
	}

	public Optional<MultiResourceHandlerEvent> sendEndAccess(String sessionId) {
		MultiResourceHandlerEventEndAccess endAccess = (MultiResourceHandlerEventEndAccess) createEndAccess(sessionId);
		try {
			MultiResourceHandlerEvent responseEvent = (MultiResourceHandlerEvent) componentWrapper
					.getDsaAdapterEventUtils().sendEvent(componentWrapper.getEventHandler(), endAccess);
			log.info(responseEvent::toString);
			return Optional.of(responseEvent);
		} catch (InterruptedException e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
			Thread.currentThread().interrupt();
		} catch (DsaAdapterException e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
		}
		return Optional.empty();
	}
}
