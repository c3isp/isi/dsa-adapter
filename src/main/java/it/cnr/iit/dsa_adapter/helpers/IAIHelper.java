package it.cnr.iit.dsa_adapter.helpers;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class IAIHelper {

	private static final Logger log = Logger.getLogger(IAIHelper.class.getName());

	@Value(value = "${security.user.name}")
	private String restUser;
	@Value(value = "${security.user.password}")
	private String restPassword;
	@Value(value = "${api.iai.uri}")
	private String iaiUrl;
	@Value(value = "${api.iai.stopanalytic}")
	private String stopAnalytic;

	@Autowired
	private RestTemplate restTemplate;

	public IAIHelper() {
		// TODO Auto-generated constructor stub
	}

	public String callIAIStopAnalytic(String sessionId) {

//		MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();
//		body.add("sessionId", sessionId);
		HttpHeaders headers = getHeaders();
		HttpEntity<String> request = new HttpEntity<>(sessionId, headers);
		StringBuilder builder = new StringBuilder();
		String url = builder.append(iaiUrl).append(stopAnalytic).toString();
		log.severe("calling " + url);
		ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);
		return response.getBody();

	}

	private HttpHeaders getHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setBasicAuth(restUser, restPassword);
		return headers;
	}

}
