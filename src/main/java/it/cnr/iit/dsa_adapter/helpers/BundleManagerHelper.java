package it.cnr.iit.dsa_adapter.helpers;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.cnr.iit.common.eventhandler.events.Event;
import it.cnr.iit.common.eventhandler.events.bundlemanager.BundleManagerCreateEvent;
import it.cnr.iit.common.eventhandler.events.bundlemanager.BundleManagerDeleteEvent;
import it.cnr.iit.common.eventhandler.events.bundlemanager.BundleManagerReadEvent;
import it.cnr.iit.common.eventhandler.events.bundlemanager.BundleManagerReadResponseEvent;
import it.cnr.iit.common.utility.JsonUtility;
import it.cnr.iit.dsa_adapter.impl.DsaAdapterException;
import it.cnr.iit.utils.ComponentWrapper;

@Component
public class BundleManagerHelper {

	private static final Logger log = Logger.getLogger(BundleManagerHelper.class.getName());

	@Autowired
	private ComponentWrapper componentWrapper;

	public Map<String, File[]> getDpoFilesMap(String[] dpoIDs, String dataLakeUri) throws DsaAdapterException {
		Map<String, File[]> map = new HashMap<>();
		for (String dpoId : dpoIDs) {
			BundleManagerReadResponseEvent responseEvent = read(dpoId);
			try {
				File[] bundleContent = componentWrapper.getDsaAdapterFileUtils()
						.extractBundleReadResponse(responseEvent, dataLakeUri);

				map.put(dpoId, bundleContent);
			} catch (Exception e) {
				throw new DsaAdapterException(
						e.getClass().getSimpleName() + " extracting " + dpoId + " : " + e.getMessage());
			}
		}
		return map;
	}

	public BundleManagerReadResponseEvent read(String dpoId) throws DsaAdapterException {
		BundleManagerReadEvent event = createEventBundleManagerRead(dpoId);
		try {
			Event responseEvent = componentWrapper.getDsaAdapterEventUtils()
					.sendEvent(componentWrapper.getEventHandler(), event);

			return (BundleManagerReadResponseEvent) responseEvent;
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new DsaAdapterException(e.getClass().getSimpleName());
		}
	}

	public static BundleManagerCreateEvent createEventBundleManagerCreate(String metadata, String encryptedFilePath,
			String tmpKey) throws IOException {
		Map<String, String> map = JsonUtility.loadObjectFromJsonString(metadata, HashMap.class).get();
//		map.put("id", String.valueOf(UUID.randomUUID().toString()));
		String dsaId = map.get("dsa_id");
//		String fileContentsOMG = new String(Base64.getEncoder().encode(FileUtils.readFileToByteArray(fileToSend)),
//				StandardCharsets.ISO_8859_1);
		return new BundleManagerCreateEvent(dsaId, metadata, encryptedFilePath, tmpKey);
	}

	public static BundleManagerReadEvent createEventBundleManagerRead(String dposId) {
		String uuid = UUID.randomUUID().toString();
		return new BundleManagerReadEvent(dposId, uuid, uuid);
	}

	public static BundleManagerDeleteEvent createEventBundleManagerDelete(String dposId) {
		String uuid = UUID.randomUUID().toString();
		return new BundleManagerDeleteEvent(dposId, uuid);
	}

}
