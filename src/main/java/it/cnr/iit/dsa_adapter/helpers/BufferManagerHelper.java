package it.cnr.iit.dsa_adapter.helpers;

import java.net.URI;
import java.util.Base64;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.common.eventhandler.events.ResponseEvent;
import it.cnr.iit.dsa_adapter.enums.DATA_LAKE;
import it.cnr.iit.dsa_adapter.enums.DATA_TYPE;
import it.cnr.iit.dsa_adapter.enums.FORMAT;
import it.cnr.iit.utils.ComponentWrapper;

@Component
public class BufferManagerHelper {

	private static final Logger log = Logger.getLogger(BundleManagerHelper.class.getName());

	@Value(value = "${security.user.name}")
	private String restUser;
	@Value(value = "${security.user.password}")
	private String restPassword;

	@Autowired
	private ComponentWrapper componentWrapper;

	@Autowired
	private RestTemplate restTemplate;

	public static final String BUFFER_MANAGER_METADATA_HEADER = "X-c3isp-metadata";

	public Optional<String> createEmptyDataLake() {
		String uri = componentWrapper.getBufferManagerProperties().getPrepareEmptyDatalakeAPI(DATA_LAKE.VDL,
				DATA_TYPE.FS);
		try {
			HttpHeaders headers = getHeaders();
			HttpEntity<String> request = new HttpEntity<>(headers);
			ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, request, String.class);
			JsonNode jsonNode = new ObjectMapper().readTree(response.getBody());
			if (response.getStatusCode() == HttpStatus.CREATED
					&& jsonNode.path("message").asText().contains("Success")) {
				return Optional.of(jsonNode.path("URI").asText());
			}
		} catch (Exception e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
		}
		return Optional.empty();
	}

	public String prepareData(Set<String> dpoIdSet, String metadata, String serviceName, DATA_LAKE dataLake,
			DATA_TYPE dataType, FORMAT format) {
		String uri = componentWrapper.getBufferManagerProperties().getPrepareDataAPI(serviceName, dataLake, dataType,
				format);
		try {
			HttpHeaders headers = getHeaders();
			headers.add(BUFFER_MANAGER_METADATA_HEADER, metadata);
			HttpEntity<String> request = new HttpEntity<>(dpoIdSet, (MultiValueMap) headers);
			ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, request, String.class);
			JsonNode jsonNode = new ObjectMapper().readTree(response.getBody());
			if (response.getStatusCode() == HttpStatus.CREATED
					&& jsonNode.path("message").asText().contains("Success")) {
				return jsonNode.path("URI").asText();
			}
			return response.getBody();
		} catch (Exception e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public ResponseEvent releaseData(String datalakeUri) {
		String plainCreds = restUser + ":" + restPassword;
		String base64Creds = new String(Base64.getEncoder().encode(plainCreds.getBytes()));

		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.add("Authorization", "Basic " + base64Creds);
		URI endpoint = UriComponentsBuilder.fromUriString(componentWrapper.getBufferManagerProperties().getUri())
				.path("/v1/releaseData").build().toUri();

		HttpEntity<String> request = new HttpEntity<String>(datalakeUri, header);
		String res = restTemplate.postForObject(endpoint, request, String.class);

		if (res.toLowerCase().contains("successfully removed")) {
			return new ResponseEvent(true);
		} else {
			return new ResponseEvent(false, res);
		}
	}

	private HttpHeaders getHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setBasicAuth(restUser, restPassword);
		headers.add("Content-Type", "application/json");
		return headers;
	}

}
