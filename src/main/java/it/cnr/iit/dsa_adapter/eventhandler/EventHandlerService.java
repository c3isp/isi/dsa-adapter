package it.cnr.iit.dsa_adapter.eventhandler;

import java.util.HashMap;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import it.cnr.iit.common.eventhandler.events.RequestEvent;
import it.cnr.iit.common.eventhandler.events.ResponseEvent;
import it.cnr.iit.common.eventhandler.events.bundlemanager.BundleManagerCreateResponseEvent;
import it.cnr.iit.common.eventhandler.events.bundlemanager.BundleManagerDeleteResponseEvent;
import it.cnr.iit.common.eventhandler.events.bundlemanager.BundleManagerEventConstants;
import it.cnr.iit.common.eventhandler.events.bundlemanager.BundleManagerReadResponseEvent;
import it.cnr.iit.common.eventhandler.events.multiresourcehandler.MultiResourceHandlerEvent;
import it.cnr.iit.common.reject.Reject;
import it.cnr.iit.dsa_adapter.impl.DsaAdapterException;
import it.cnr.iit.utils.ComponentWrapper;

@Service
public class EventHandlerService {

	private static final Logger log = Logger.getLogger(EventHandlerService.class.getName());

	@Autowired
	private ComponentWrapper componentWrapper;

	@Autowired
	private RestTemplate restTemplate;

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	private HashMap<String, BlockingQueue<RequestEvent>> queueMap;
	private ConcurrentHashMap<String, RequestEvent> endSessionMap;

	public EventHandlerService() {
		queueMap = new HashMap<>();
		endSessionMap = new ConcurrentHashMap<>();
	}

	public boolean receiveEvent(RequestEvent event) throws InterruptedException {
		log.log(Level.SEVERE, "received event " + event.getEventType() + " -> queue size : {0}", queueMap.size());
		BlockingQueue<RequestEvent> queue;

		switch (event.getEventType()) {

		case "nc": {// tryaccess response
			MultiResourceHandlerEvent mrhEvent = new MultiResourceHandlerEvent();
			mrhEvent.setProperty("response", event.getProperty("response"));
			mrhEvent.setProperty("longResponse", event.getProperty("longResponse"));
			if (event.getProperty("response_link") != null) {
				mrhEvent.setProperty("response_link", event.getProperty("response_link"));
			}

			mrhEvent.setSessionID(event.getProperty("sessionID"));
			mrhEvent.setEventType("nc");
			mrhEvent.setAdditionalProperties(event.getAdditionalProperties());

//			queue = queueMap.get(mrhEvent.getSessionID());
			queue = queueMap.get(mrhEvent.getRequestID());
			if (queue == null) {
				log.severe(() -> "ucs event has no associated messageId in queue: sessionId " + mrhEvent.getRequestID()
						+ ", size " + queueMap.size());
				return false;
			}
			queue.put(mrhEvent);
			return true;
		}

		case "ncm": {// tryaccess_multi response
			log.severe("received event tryAccessResponseMulti");
			MultiResourceHandlerEvent mrhEvent = new MultiResourceHandlerEvent();
			mrhEvent.setProperty("response_link", event.getProperty("response_link"));
			mrhEvent.setSessionID(event.getProperty("sessionID"));
			mrhEvent.setEventType("ncm");
			log.severe("in ncm case, requestID=" + mrhEvent.getRequestID());
			queueMap.entrySet().stream().forEach(e -> log.severe("ID in queue: " + e.getKey()));
			queue = queueMap.get(event.getRequestID());
			if (queue == null) {
				log.severe(() -> "ucs event has no associated messageId in queue: sessionId " // +
																								// mrhEvent.getSessionID()
						+ ", size " + queueMap.size());
				return false;
			}
			queue.put(mrhEvent);
			return true;
		}
		case "er": {// endaccess response
			log.severe("inside endAccess response event (er)");
			MultiResourceHandlerEvent mrhEvent = new MultiResourceHandlerEvent();
			mrhEvent.setProperty("response", event.getProperty("response"));
			mrhEvent.setProperty("longResponse", event.getProperty("longResponse"));
			mrhEvent.setProperty("result", event.getProperty("result"));
			mrhEvent.setSessionID(event.getProperty("sessionID"));
			mrhEvent.setEventType("er");
			if (event.hasProperty("revoke")) {
				log.severe("setting property revoke = true");
				mrhEvent.setProperty("revoke", event.getProperty("revoke"));
				log.severe("calling stopAnalytic");
				String iaiResponse = componentWrapper.getIaiHelper()
						.callIAIStopAnalytic(event.getProperty("sessionID"));
				log.severe("iaiResponse=" + iaiResponse);
			}
//			queue = queueMap.get(event.getRequestID());
			queue = queueMap.get(mrhEvent.getSessionID());
			if (queue == null) {
				log.severe(() -> "ucs event has no associated messageId in queue: sessionId " // +
																								// mrhEvent.getSessionID()
						+ ", size " + queueMap.size());
				return false;
			}
			queue.put(mrhEvent);
			return true;
		}
		case "r": { // revoke
			MultiResourceHandlerEvent mrhEvent = (MultiResourceHandlerEvent) event;
			String sessionId = mrhEvent.getSessionID();
			Reject.ifNull(sessionId, "usage control event received without sessionId");
			RequestEvent prevEvent = endSessionMap.put(sessionId, mrhEvent);
			if (prevEvent == null) {
				log.log(Level.FINEST, () -> "no previous events existed for sessionId: " + sessionId);
			}
			return true;
		}
		case "bmcr": { // bundleManager create response
			BundleManagerCreateResponseEvent bmcr = new BundleManagerCreateResponseEvent(event.getProperty("dposId"),
					event.getRequestID());
			queueMap.get(bmcr.getRequestID()).put(bmcr);
			return true;
		}
		case "bmrr": {// bundleManager read response
			BundleManagerReadResponseEvent bmrr = new BundleManagerReadResponseEvent(
					event.getProperty(BundleManagerEventConstants.CONTENT_URI),
					event.getProperty(BundleManagerEventConstants.POLICY_URI),
					event.getProperty(BundleManagerEventConstants.METADATA_URI), "ClearFormat",
					event.getProperty(BundleManagerEventConstants.REQUEST_ID));
			queueMap.get(bmrr.getRequestID()).put(bmrr);
			return true;
		}
		case "bmdr": { // bundleManager delete response
			BundleManagerDeleteResponseEvent bmdr = new BundleManagerDeleteResponseEvent(event.getProperty("result"),
					event.getRequestID());
			queueMap.get(bmdr.getRequestID()).put(bmdr);
			return true;
		}
		default:
			log.severe("received unexpected event type: " + event);
		}

		return false;
	}

	public BlockingQueue<RequestEvent> notify(RequestEvent event) {
		Reject.ifNull(event);
		try {
			String id = Optional
					.of(event.getRequestID() != null ? event.getRequestID()
							: (event.getProperty("sessionID") != null ? event.getProperty("sessionID")
									: event.getMessageID()))
					.orElseThrow(() -> new DsaAdapterException(
							"Cannot retrieve the id from event of type " + event.getEventType()));

			log.severe("putting in queue with id = " + id);
			BlockingQueue<RequestEvent> queue = new LinkedBlockingQueue<RequestEvent>();
			queueMap.put(id, queue);
			if (!sendEvent(event)) {
				log.severe("error notifyEventHandler, no success in response.");
			}
			return queue;
		} catch (Exception e) {
			log.severe(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	private boolean sendEvent(RequestEvent event) {
		String url = componentWrapper.getEventHandlerProperties().getEventHandlerNotifyEventAPI();
		ResponseEvent responseEvent = restTemplate.postForObject(url, event, ResponseEvent.class);
		return responseEvent.isSuccess();
	}

	public HashMap<String, BlockingQueue<RequestEvent>> getQueueMap() {
		return queueMap;
	}

	public void setQueueMap(HashMap<String, BlockingQueue<RequestEvent>> queueMap) {
		this.queueMap = queueMap;
	}

}
