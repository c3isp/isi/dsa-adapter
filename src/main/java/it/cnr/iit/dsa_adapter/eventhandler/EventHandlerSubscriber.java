package it.cnr.iit.dsa_adapter.eventhandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import it.cnr.iit.common.eventhandler.AbstractEventHandlerSubscriber;
import it.cnr.iit.common.eventhandler.events.bundlemanager.BundleManagerEventTypes;
import it.cnr.iit.common.eventhandler.events.multiresourcehandler.MultiResourceHandlerEventTypes;
import it.cnr.iit.utils.ComponentWrapper;

@Configuration
public class EventHandlerSubscriber extends AbstractEventHandlerSubscriber {

	@Autowired
	private ComponentWrapper componentWrapper;

	public EventHandlerSubscriber() {
		super(new String[] { MultiResourceHandlerEventTypes.TRY_ACCESS_RESPONSE,
				MultiResourceHandlerEventTypes.TRY_ACCESS_MULTI_RESPONSE, MultiResourceHandlerEventTypes.REVOKE_ACCESS,
				MultiResourceHandlerEventTypes.END_ACCESS_RESPONSE, BundleManagerEventTypes.CREATE_RESPONSE,
				BundleManagerEventTypes.DELETE_RESPONSE, BundleManagerEventTypes.READ_RESPONSE });
		setScheduleRate(15, 120);
		start();
	}

	@Override
	public String getEventHandlerSubscribeAPI() {
		return componentWrapper.getEventHandlerProperties().getEventHandlerSubscribeAPI();
	}

	@Override
	public String getEventHandlerSubscribersAPI() {
		return componentWrapper.getEventHandlerProperties().getEventHandlerSubscribersAPI();
	}

	@Override
	public String getSelfEventArrivedAPI() {
		return componentWrapper.getDsaAdapterProperties().getNotifyEventAPI();
	}

}
