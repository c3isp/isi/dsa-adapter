package it.cnr.iit.dsa_adapter.impl;

public class DsaAdapterException extends Exception {

	private static final long serialVersionUID = 1L;

	public DsaAdapterException(String message) {
		super(message);
	}

}
