package it.cnr.iit.dsa_adapter.impl;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.att.research.xacml.api.Request;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.cnr.iit.common.attributes.AttributeIds;
import it.cnr.iit.common.eventhandler.events.Event;
import it.cnr.iit.common.eventhandler.events.RequestEvent;
import it.cnr.iit.common.eventhandler.events.ResponseEvent;
import it.cnr.iit.common.eventhandler.events.bundlemanager.BundleManagerCreateEvent;
import it.cnr.iit.common.eventhandler.events.bundlemanager.BundleManagerDeleteEvent;
import it.cnr.iit.common.eventhandler.events.bundlemanager.BundleManagerReadEvent;
import it.cnr.iit.common.eventhandler.events.bundlemanager.BundleManagerReadResponseEvent;
import it.cnr.iit.common.eventhandler.events.multiresourcehandler.MultiResourceHandlerEvent;
import it.cnr.iit.common.types.DataManipulationObject;
import it.cnr.iit.common.types.Metadata;
import it.cnr.iit.common.utility.FileUtility;
import it.cnr.iit.dsa_adapter.db.SessionRecorder;
import it.cnr.iit.dsa_adapter.enums.DATA_LAKE;
import it.cnr.iit.dsa_adapter.enums.DATA_TYPE;
import it.cnr.iit.dsa_adapter.enums.FORMAT;
import it.cnr.iit.dsa_adapter.helpers.BundleManagerHelper;
import it.cnr.iit.obligationengine.obligations.DMOExecution;
import it.cnr.iit.utils.ComponentWrapper;
import it.cnr.iit.xacml.RequestGenerator;
import it.cnr.iit.xacml.RequestSerializationFactory;
import it.cnr.iit.xacml.attribute.AdditionalAttribute;
import it.cnr.iit.xacml.request_container.RequestAttributes;
import it.cnr.iit.xacml.request_container.RequestContainer;

@Service
public class DsaAdapterService {

	private static final Logger log = Logger.getLogger(DsaAdapterService.class.getName());

	@Autowired
	private ComponentWrapper componentWrapper;

	public String[] checkAvailableDpos(String[] dpoIDs, String metadata, String action) throws DsaAdapterException {

		log.severe(() -> "calling checkAvailableDpos for " + action + " -> " + dpoIDs.length + " dpos : "
				+ Arrays.toString(dpoIDs) + " metadata : " + metadata);

		String dataLakeUri = componentWrapper.getBufferManager().createEmptyDataLake()
				.orElseThrow(() -> new DsaAdapterException("Error creating an empty data lake"));
		log.severe(() -> "bufferManager -> dataLakeUri : " + dataLakeUri);
		File dataLakeFile = FileUtility.getFile(dataLakeUri)
				.orElseThrow(() -> new DsaAdapterException("Error accessing the empty data lake"));

		Map<String, File[]> dpoFilesMap = componentWrapper.getBundleManager().getDpoFilesMap(dpoIDs, dataLakeUri);

		RequestContainer requestContainer = null;
		try {
			requestContainer = new ObjectMapper().readValue(metadata, RequestContainer.class);
		} catch (JsonProcessingException e2) {
			e2.printStackTrace();
		}

		List<RequestAttributes> metadataAttributes = requestContainer.getRequest().getAttributes();
		Vector<AdditionalAttribute> attrVect = componentWrapper.getDsaAdapterAttributeUtils()
				.generateAttrVect(metadataAttributes, action);

		String sessionId = "111";

		AdditionalAttribute[] addAttrArray = attrVect.toArray(new AdditionalAttribute[attrVect.size()]);

		List<AdditionalAttribute> aaList = new ArrayList<AdditionalAttribute>();
		aaList.addAll(Arrays.asList(addAttrArray));

		List<String> policyFileList = componentWrapper.getDsaAdapterPolicyRequestUtils().getPolicyFiles(dataLakeFile,
				dpoFilesMap);
		List<String> requestFileList = componentWrapper.getDsaAdapterPolicyRequestUtils().getRequestFiles(dataLakeFile,
				aaList, dpoFilesMap);

		MultiResourceHandlerEvent mrhResponseEvent = componentWrapper.getMultiResourceHandler()
				.checkDpos(sessionId, policyFileList, requestFileList, true)
				.orElseThrow(() -> new DsaAdapterException("Error calling MultiResourceHandler"));

		File mrhResponse = FileUtility.getFile("file://" + mrhResponseEvent.getProperty("response_link"))
				.orElseThrow(() -> new DsaAdapterException("Error accessing the empty data lake"));

		List<String> deniedDpos = componentWrapper.getDsaManager().filterDenyResponses(mrhResponse);
		deniedDpos.stream().forEach(el -> log.severe("deniedDpos: " + el));
		for (String s : deniedDpos) {
			dpoFilesMap.remove(s);
		}

		if (dpoFilesMap.isEmpty()) {
			log.severe("dpoFileMap is empty");
			return null;
		}

		List<String> permittedDpos = new ArrayList<String>();
		for (Map.Entry<String, File[]> entry : dpoFilesMap.entrySet()) {
			permittedDpos.add(entry.getKey());
		}

		return (String[]) permittedDpos.toArray();
	}

	public String prepareData(String[] dpoIDs, String metadata, String serviceName, FORMAT format, DATA_LAKE dataLake,
			DATA_TYPE dataType) throws DsaAdapterException, Exception {
		log.severe(() -> "calling prepareData for " + serviceName + " -> " + dpoIDs.length + " dpos : "
				+ Arrays.toString(dpoIDs) + " metadata : " + metadata);

		String dataLakeUri = componentWrapper.getBufferManager().createEmptyDataLake()
				.orElseThrow(() -> new DsaAdapterException("Error creating an empty data lake"));
		log.severe(() -> "bufferManager -> dataLakeUri : " + dataLakeUri);
		File dataLakeFile = FileUtility.getFile(dataLakeUri)
				.orElseThrow(() -> new DsaAdapterException("Error accessing the empty data lake"));

		Map<String, File[]> dpoFilesMap = componentWrapper.getBundleManager().getDpoFilesMap(dpoIDs, dataLakeUri);

		RequestContainer requestContainer = new ObjectMapper().readValue(metadata, RequestContainer.class);

		List<RequestAttributes> metadataAttributes = requestContainer.getRequest().getAttributes();
		Vector<AdditionalAttribute> attrVect = componentWrapper.getDsaAdapterAttributeUtils()
				.generateAttrVect(metadataAttributes, serviceName);

		String sessionId = componentWrapper.getDsaAdapterAttributeUtils()
				.findRequestAttribute(metadataAttributes, AttributeIds.IAI_SESSION_ID).getValue();

		AdditionalAttribute[] addAttrArray = attrVect.toArray(new AdditionalAttribute[attrVect.size()]);

		List<AdditionalAttribute> aaList = new ArrayList<AdditionalAttribute>();
		aaList.addAll(Arrays.asList(addAttrArray));

		SessionRecorder sessionRec = new SessionRecorder();
		sessionRec.setSession_id(sessionId);
		sessionRec.setDatalake_uri(dataLakeUri);
		componentWrapper.getSessionRecorderStorage().createOrUpdate(sessionRec);

		List<String> policyFileList = componentWrapper.getDsaAdapterPolicyRequestUtils().getPolicyFiles(dataLakeFile,
				dpoFilesMap);
		List<String> requestFileList = componentWrapper.getDsaAdapterPolicyRequestUtils().getRequestFiles(dataLakeFile,
				aaList, dpoFilesMap);

		MultiResourceHandlerEvent mrhResponseEvent = componentWrapper.getMultiResourceHandler()
				.checkDpos(sessionId, policyFileList, requestFileList, true)
				.orElseThrow(() -> new DsaAdapterException("Error calling MultiResourceHandler"));

		File mrhResponse = FileUtility.getFile("file://" + mrhResponseEvent.getProperty("response_link"))
				.orElseThrow(() -> new DsaAdapterException("Error accessing the empty data lake"));

		List<String> deniedDpos = componentWrapper.getDsaManager().filterDenyResponses(mrhResponse);
		deniedDpos.stream().forEach(el -> log.severe("deniedDpos: " + el));
		for (String s : deniedDpos) {
			dpoFilesMap.remove(s);
		}

		if (dpoFilesMap.isEmpty()) {
			log.severe("dpoFileMap is empty");
			return null;
		}

		String prepareDataUri = null;
		try {
			prepareDataUri = componentWrapper.getBufferManager().prepareData(dpoFilesMap.keySet(), metadata,
					serviceName, dataLake, dataType, format);
			log.severe("bufferManager -> prepareDataUri: " + prepareDataUri);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// TODO
		// need to test this solution for multiple DPOs

		File metadataFile = dpoFilesMap.get(dpoIDs[0])[2];
		Metadata meta = new ObjectMapper().readValue(new String(Files.readAllBytes(Paths.get(metadataFile.getPath()))),
				Metadata.class);

		JsonNode obj = new ObjectMapper().readTree(prepareDataUri);
		log.severe("prepareData mapped result before = " + new ObjectMapper().writeValueAsString(obj));
		((ObjectNode) obj).put("file:extension", meta.getExtension());
		log.severe("prepareData mapped result after = " + new ObjectMapper().writeValueAsString(obj));
		return new ObjectMapper().writeValueAsString(obj);
	}

	public ResponseEvent releaseData(String sessionId) throws DsaAdapterException {
		log.severe("Called releaseData for sessionId " + sessionId);
		SessionRecorder sessionRecorder = componentWrapper.getSessionRecorderStorage().getForSessionId(sessionId);
		String dataLakeUri = sessionRecorder.getDatalake_uri();

		ResponseEvent response = componentWrapper.getBufferManager().releaseData(dataLakeUri);

		if (componentWrapper.getEventHandler().getQueueMap().containsKey(sessionId)) {
			log.severe("found queue with sessionId = " + sessionId);
			BlockingQueue<RequestEvent> requestEventQueue = componentWrapper.getEventHandler().getQueueMap()
					.get(sessionId);
			if (requestEventQueue.stream().filter(el -> el.getEventType().equals("er")).count() == 0) {
				log.severe("no revoke inside the queue");
				componentWrapper.getMultiResourceHandler().sendEndAccess(sessionId)
						.orElseThrow(() -> new DsaAdapterException("Error calling endAccess"));

			} else {
				log.severe("EndAccess already performed from REVOKE for sessionId " + sessionId);
				response.setProperty("revoke", "true");
			}
		}

		if (response.isSuccess()) {
			log.severe("response is success. Deleting data from SessionRecorder...");
			componentWrapper.getSessionRecorderStorage().delete(sessionRecorder);
		}
		return response;
	}

	public FileSystemResource read(String dpoID, String metadata) throws DsaAdapterException, IOException {

		String dataLakeUri = componentWrapper.getBufferManager().createEmptyDataLake()
				.orElseThrow(() -> new DsaAdapterException("Error creating an empty data lake"));
		log.info(() -> "bufferManager -> dataLakeUri : " + dataLakeUri);
		File dataLakeFile = FileUtility.getFile(dataLakeUri)
				.orElseThrow(() -> new DsaAdapterException("Error accessing the empty data lake"));

		RequestContainer rc = new ObjectMapper().readValue(metadata, RequestContainer.class);
		List<RequestAttributes> metadataAttributes = rc.getRequest().getAttributes();

		String[] dpoIDs = { dpoID };

		// bundle manager read
		Map<String, File[]> dpoFilesMap = componentWrapper.getBundleManager().getDpoFilesMap(dpoIDs, dataLakeUri);

		Vector<AdditionalAttribute> attrVect = null;
		attrVect = componentWrapper.getDsaAdapterAttributeUtils().generateAttrVect(metadataAttributes, "read");
		AdditionalAttribute[] addAttrArray = attrVect.toArray(new AdditionalAttribute[attrVect.size()]);

		List<AdditionalAttribute> aaList = new ArrayList<AdditionalAttribute>();
		aaList.addAll(Arrays.asList(addAttrArray));

		List<String> policyFile = componentWrapper.getDsaAdapterPolicyRequestUtils().getPolicyFiles(dataLakeFile,
				dpoFilesMap);
		List<String> requestFile = componentWrapper.getDsaAdapterPolicyRequestUtils().getRequestFiles(dataLakeFile,
				aaList, dpoFilesMap);

		MultiResourceHandlerEvent mrhResponseEvent = componentWrapper.getMultiResourceHandler()
				.checkDpos(null, policyFile, requestFile, false)
				.orElseThrow(() -> new DsaAdapterException("Error calling MultiResourceHandler"));

		if (mrhResponseEvent.getProperty("response").contains("false")) {
			log.severe("tryAccess response received is deny");
			return null;
		}

		List<DataManipulationObject> dmos = componentWrapper.getDsaAdapterEventUtils().extractDmo(mrhResponseEvent,
				dpoID, "read", dpoFilesMap.get(dpoID)[1]);
		log.severe("dsa received the following dmos to execute: " + new ObjectMapper().writeValueAsString(dmos));

		File contentFile = dpoFilesMap.get(dpoID)[1];

		for (DataManipulationObject dmo : dmos) {
			String output = DMOExecution.executeDMO(dmo);
			if (output != null) {
				FileUtils.writeStringToFile(contentFile, output, StandardCharsets.UTF_8);
			}
		}

		return new FileSystemResource(contentFile);
	}

	public ResponseEvent create(String dposMetadata, String cipheredFilePath, String tmpSymmetricalKey)
			throws IOException {

		log.severe("in dsaAdapter create, metadata = " + dposMetadata);
		RequestContainer rc = new ObjectMapper().readValue(dposMetadata, RequestContainer.class);
		List<RequestAttributes> metadataAttributes = rc.getRequest().getAttributes();

		Metadata cm = componentWrapper.getDsaManager().getMetadata(metadataAttributes);

		String dsaId = componentWrapper.getDsaAdapterAttributeUtils()
				.findRequestAttribute(metadataAttributes, AttributeIds.DSA_ID).getValue();

		String policy = componentWrapper.getDsaManager().getPolicy(dsaId);
		cm.setId(System.currentTimeMillis() + "-" + UUID.randomUUID().toString());

		Vector<AdditionalAttribute> attrVect = componentWrapper.getDsaAdapterAttributeUtils()
				.generateAttrVect(metadataAttributes, "create");
		AdditionalAttribute[] addAttrArray = attrVect.toArray(new AdditionalAttribute[attrVect.size()]);

		Request request = new RequestGenerator().createXACMLV3Request(true, addAttrArray);
		String xacmlRequest = RequestSerializationFactory.newInstance().serialize(request,
				RequestSerializationFactory.SERIALIZATION_FORMAT.XML);

		List<String> policyList = new ArrayList<String>();
		List<String> requestList = new ArrayList<String>();
		policyList.add(policy);
		requestList.add(xacmlRequest);

		MultiResourceHandlerEvent mrhResponseEvent = null;
		try {
			mrhResponseEvent = componentWrapper.getMultiResourceHandler()
					.checkDpos(null, policyList, requestList, false)
					.orElseThrow(() -> new DsaAdapterException("Error calling MultiResourceHandler"));

			if (mrhResponseEvent.getProperty("response") == "false") {
				log.severe("tryAccess response received is deny");
				return new ResponseEvent(false, "tryAccess response received is deny");
			}
		} catch (DsaAdapterException e) {
			e.printStackTrace();
			return new ResponseEvent(false, e.getMessage());
		}

		for (RequestAttributes ra : metadataAttributes) {
			if (ra.getAttributeId().equals(AttributeIds.EXTENSION)) {
				cm.setExtension(ra.getValue());
			}
		}

		BundleManagerCreateEvent bmc = BundleManagerHelper.createEventBundleManagerCreate(
				new ObjectMapper().writeValueAsString(cm), cipheredFilePath, tmpSymmetricalKey);

		Event responseEvent = null;

		log.severe("in create, DSA adapter sending the following event to the bundle manager: "
				+ new ObjectMapper().writeValueAsString(bmc));

		try {
			responseEvent = componentWrapper.getDsaAdapterEventUtils().sendEvent(componentWrapper.getEventHandler(),
					bmc);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEvent(false, e.getMessage());
		}

		ResponseEvent res = new ResponseEvent(true);
		res.setProperty("dposId", responseEvent.getProperty("dposId"));

		BundleManagerReadEvent event = componentWrapper.getBundleManager()
				.createEventBundleManagerRead(responseEvent.getProperty("dposId"));

		BundleManagerReadResponseEvent bmResponse = null;
		try {
			bmResponse = (BundleManagerReadResponseEvent) componentWrapper.getDsaAdapterEventUtils()
					.sendEvent(componentWrapper.getEventHandler(), event);
		} catch (DsaAdapterException | InterruptedException e) {
			e.printStackTrace();
		}

		File content = componentWrapper.getDsaAdapterFileUtils().exctractBundleReadResponseContent(bmResponse);

		List<DataManipulationObject> dmos = componentWrapper.getDsaAdapterEventUtils().extractDmo(mrhResponseEvent,
				responseEvent.getProperty("dposId"), "create", content);
		log.severe("dsa received the following dmos to execute: " + new ObjectMapper().writeValueAsString(dmos));

		for (DataManipulationObject dmo : dmos) {
			DMOExecution.executeDMO(dmo);
		}

		return res;
	}

	public ResponseEvent delete(String dpoId, String dpoMetadata) throws IOException {

		log.severe("Called delete for dpo " + dpoId);

		RequestContainer rc = new ObjectMapper().readValue(dpoMetadata, RequestContainer.class);
		List<RequestAttributes> metadataAttributes = rc.getRequest().getAttributes();
		Metadata metadata = componentWrapper.getDsaManager().getMetadata(metadataAttributes);

		String dsaId = metadata.getDsaId();
		String policy = componentWrapper.getDsaManager().getPolicy(dsaId);

		Vector<AdditionalAttribute> attrVect = componentWrapper.getDsaAdapterAttributeUtils()
				.generateAttrVect(metadataAttributes, "delete");
		AdditionalAttribute[] addAttrArray = attrVect.toArray(new AdditionalAttribute[attrVect.size()]);

		Request request = new RequestGenerator().createXACMLV3Request(true, addAttrArray);
		String xacmlRequest = RequestSerializationFactory.newInstance().serialize(request,
				RequestSerializationFactory.SERIALIZATION_FORMAT.XML);

		List<String> policyList = new ArrayList<String>();
		List<String> requestList = new ArrayList<String>();
		policyList.add(policy);
		requestList.add(xacmlRequest);

		MultiResourceHandlerEvent mrhResponseEvent = null;
		try {
			mrhResponseEvent = componentWrapper.getMultiResourceHandler()
					.checkDpos(null, policyList, requestList, false)
					.orElseThrow(() -> new DsaAdapterException("Error calling MultiResourceHandler"));
			// if permit
			if (mrhResponseEvent.getProperty("response") == "false") {
				log.severe("tryAccess response received is DENY for delete operation");
				return new ResponseEvent(false);
			}
		} catch (DsaAdapterException e) {
			e.printStackTrace();
		}

		BundleManagerDeleteEvent bmd = BundleManagerHelper.createEventBundleManagerDelete(dpoId);
		Event responseEvent = null;
		try {
			responseEvent = componentWrapper.getDsaAdapterEventUtils().sendEvent(componentWrapper.getEventHandler(),
					bmd);
		} catch (Exception e) {
			log.severe("error in receiving responseEvent to delete: " + e.getStackTrace());
			e.printStackTrace();
		}

		log.severe("dsaAdapter responseEvent to delete: " + responseEvent);

		List<DataManipulationObject> dmos = componentWrapper.getDsaAdapterEventUtils().extractDmo(mrhResponseEvent,
				dpoId, "delete", null);
		log.severe("dsa received the following dmos to execute: " + new ObjectMapper().writeValueAsString(dmos));

		for (DataManipulationObject dmo : dmos) {
			DMOExecution.executeDMO(dmo);
		}

		return new ResponseEvent(true);
	}

	public ResponseEvent move(String dpoId, String destination, String metadata)
			throws IOException, DsaAdapterException {

		log.severe("Called move for dpo " + dpoId + " wth destination " + destination + " and metadata: " + metadata);

		String dataLakeUri = componentWrapper.getBufferManager().createEmptyDataLake()
				.orElseThrow(() -> new DsaAdapterException("Error creating an empty data lake"));
		log.info(() -> "bufferManager -> dataLakeUri : " + dataLakeUri);
		File dataLakeFile = FileUtility.getFile(dataLakeUri)
				.orElseThrow(() -> new DsaAdapterException("Error accessing the empty data lake"));

		RequestContainer rc = new ObjectMapper().readValue(metadata, RequestContainer.class);
		List<RequestAttributes> metadataAttributes = rc.getRequest().getAttributes();

		String[] dpoIDs = { dpoId };

		// bundle manager read
		Map<String, File[]> dpoFilesMap = componentWrapper.getBundleManager().getDpoFilesMap(dpoIDs, dataLakeUri);

		Vector<AdditionalAttribute> attrVect = null;
		attrVect = componentWrapper.getDsaAdapterAttributeUtils().generateAttrVect(metadataAttributes, "read");
		AdditionalAttribute[] addAttrArray = attrVect.toArray(new AdditionalAttribute[attrVect.size()]);

		List<AdditionalAttribute> aaList = new ArrayList<AdditionalAttribute>();
		aaList.addAll(Arrays.asList(addAttrArray));

		List<String> policyFile = componentWrapper.getDsaAdapterPolicyRequestUtils().getPolicyFiles(dataLakeFile,
				dpoFilesMap);
		List<String> requestFile = componentWrapper.getDsaAdapterPolicyRequestUtils().getRequestFiles(dataLakeFile,
				aaList, dpoFilesMap);

		MultiResourceHandlerEvent mrhResponseEvent = componentWrapper.getMultiResourceHandler()
				.checkDpos(null, policyFile, requestFile, false)
				.orElseThrow(() -> new DsaAdapterException("Error calling MultiResourceHandler"));

		log.severe("after calling mrh, mrhResponseEvent = " + new ObjectMapper().writeValueAsString(mrhResponseEvent));

		if (mrhResponseEvent.getProperty("response").contains("false")) {
			log.severe("tryAccess response received is deny");
			return null;
		}

		ResponseEntity<ResponseEvent> response = componentWrapper.getMoveHelper().sendDPO(dpoFilesMap.get(dpoId)[1],
				metadata, destination);
		if (!response.getStatusCode().is2xxSuccessful()) {
			return new ResponseEvent(false);
		}
		ResponseEvent res = response.getBody();
		if (!delete(dpoId, metadata).isSuccess()) {
			log.severe("Error deleting DPO after move operation for dpoId: " + dpoId);
		}

		List<DataManipulationObject> dmos = componentWrapper.getDsaAdapterEventUtils().extractDmo(mrhResponseEvent,
				dpoId, "move", null);
		log.severe("dsa received the following dmos to execute: " + new ObjectMapper().writeValueAsString(dmos));

		for (DataManipulationObject dmo : dmos) {
			DMOExecution.executeDMO(dmo);
		}

		return res;
	}

	public ResponseEvent receiveDPO(String metadata, MultipartFile file) throws IOException {
//		return create(metadata, file);
		return null;
	}

}
