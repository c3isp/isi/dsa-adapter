package it.cnr.iit.dsa_adapter.enums;

public enum DATA_TYPE {
    FS, MYSQL, HDFS
}
