package it.cnr.iit.dsa_adapter.enums;

public enum FORMAT {
    EML, CEF, MODEL, PCAP, BINARY
}
