package it.cnr.iit.dsa_adapter.rest;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.cnr.iit.common.eventhandler.events.RequestEvent;
import it.cnr.iit.common.eventhandler.events.ResponseEvent;
import it.cnr.iit.common.reject.exceptions.PreconditionException;
import it.cnr.iit.utils.ComponentWrapper;

@ApiModel(value = "EventHandlerReceiverController", description = "DSA Adapter's eventHandler receiver")
@RestController
public class EventReceiverController {

	private static final Logger log = Logger.getLogger(EventReceiverController.class.getName());
	public static final String RECEIVE_EVENT = "/v1/notifyEvent";

	@Autowired
	private ComponentWrapper componentWrapper;

	@ApiResponses(value = { @ApiResponse(code = 412, message = "Client Error"),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 200, message = "Request accepted") })
	@ApiOperation(httpMethod = "POST", value = "Receives event notifications from the EventHandler")
	@PostMapping(value = { RECEIVE_EVENT })
	public ResponseEntity<ResponseEvent> receiveEvent(@RequestBody(required = true) RequestEvent event) {
		log.log(Level.INFO, "receiveEvent called");

		try {
			log.severe("dsa received the following event: " + new ObjectMapper().writeValueAsString(event));
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			if (componentWrapper.getEventHandler().receiveEvent(event)) {
				return new ResponseEntity<ResponseEvent>(new ResponseEvent(true), HttpStatus.OK);
			}
		} catch (InterruptedException e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
			Thread.currentThread().interrupt();
		} catch (PreconditionException e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
			return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
		} catch (NullPointerException e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
		}

		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
