package it.cnr.iit.dsa_adapter.rest;

import java.io.IOException;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.cnr.iit.common.eventhandler.events.ResponseEvent;
import it.cnr.iit.common.types.CreateBodyParams;
import it.cnr.iit.dsa_adapter.enums.DATA_LAKE;
import it.cnr.iit.dsa_adapter.enums.DATA_TYPE;
import it.cnr.iit.dsa_adapter.enums.FORMAT;
import it.cnr.iit.dsa_adapter.impl.DsaAdapterException;
import it.cnr.iit.utils.ComponentWrapper;

@ApiModel(value = "DsaAdapterFrontend", description = "Dsa Adapter Frontend")
@RestController
@RequestMapping("/v1")
public class DsaAdapterController {

	private static final Logger LOG = Logger.getLogger(DsaAdapterController.class.getName());

	@Autowired
	private ComponentWrapper componentWrapper;

	@ApiResponses(value = { @ApiResponse(code = 403, message = "Coco Cloud Forbidden/Request Unauthorized"),
			@ApiResponse(code = 412, message = "Client Error"),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 200, message = "Request accepted") })
	@ApiOperation(httpMethod = "POST", value = "Gets a CTI inside the DPO_ID specified", tags = {
			"DSA Adapter Front-End API" }, notes = "This method returns a CTI inside a DPO")
	@RequestMapping(method = { RequestMethod.POST }, value = { "/dpo/{dpoId}" }, consumes = {
			"multipart/form-data" }, produces = { "application/octet-stream" })
	public ResponseEntity<FileSystemResource> readDPO(@PathVariable(value = "dpoId") String dposId,
			@RequestPart(value = "input_metadata") String input_metadata) throws InterruptedException {
		FileSystemResource fsr = null;
		try {
			fsr = componentWrapper.getDsaAdapter().read(dposId, input_metadata);
		} catch (Exception e) {
			String message = "Error in calling readDpo : " + e.getMessage();
			e.printStackTrace();
			new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(fsr, HttpStatus.OK);
	}

	@ApiResponses(value = { @ApiResponse(code = 403, message = "C3ISP Forbidden/Request Unauthorized"),
			@ApiResponse(code = 412, message = "Client/Parameter Error"),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 200, message = "Request sent") })
	@ApiOperation(httpMethod = "POST", value = "Delete a DPO", tags = {
			"DSA Adapter Front-End API" }, notes = "", response = ResponseEvent.class)
	@RequestMapping(method = { RequestMethod.POST }, value = { "/delete/{dpoId}" }, produces = {
			"application/json" }, consumes = { "multipart/form-data" })
	public ResponseEntity<ResponseEvent> deleteDPO(@PathVariable(value = "dpoId") String dpoId,
			@RequestPart(value = "input_metadata") String dposMetadata) {
		try {
			ResponseEvent res = componentWrapper.getDsaAdapter().delete(dpoId, dposMetadata);
			return new ResponseEntity(res, HttpStatus.OK);
		} catch (IOException e) {
			String message = "Error in calling delete on dsaAdapter :" + e.getMessage();
			LOG.severe(message);
		}
		return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ApiResponses(value = { @ApiResponse(code = 403, message = "Forbidden/Request Unauthorized"),
			@ApiResponse(code = 412, message = "Client Error"),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 200, message = "Request accepted") })
	@ApiOperation(httpMethod = "POST", value = "Moves a DPO -- part 1: sending ISI", tags = {
			"DSA Adapter Front-End API" }, notes = "This method returns a CTI inside a DPO")
	@RequestMapping(method = { RequestMethod.POST }, value = { "/move/dpo/{dpoId}" }, produces = {
			"application/json" }, consumes = { "multipart/form-data" })
	public ResponseEntity moveDPO(@PathVariable(value = "dpoId") String dposId,
			@RequestParam(value = "destination") @ApiParam(required = true, allowEmptyValue = false, example = "https://isic3isp.iit.cnr.it:8443/isi-api/api/v1") String destination,
			@RequestPart(value = "input_metadata") String input_metadata)
			throws InterruptedException, JsonProcessingException {
		try {
			ResponseEvent res = componentWrapper.getDsaAdapter().move(dposId, destination, input_metadata);
			return new ResponseEntity(res, HttpStatus.OK);
		} catch (IOException | DsaAdapterException e) {
			String message = "Error in calling move on dsaAdapter : " + e.getMessage();
			LOG.severe(message);
		}
		return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ApiResponses(value = { @ApiResponse(code = 412, message = "Client Error"),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 200, message = "Updated succesfully") })
	@ApiOperation(httpMethod = "POST", value = "Create a new DPO", response = String.class, tags = {
			"DSA Adapter Front-End API" }, notes = "TBA")
	@RequestMapping(method = { RequestMethod.POST }, value = { "/create" }, produces = {
			"application/json" }, consumes = { "application/json" })
	public ResponseEntity<ResponseEvent> create(@RequestBody CreateBodyParams bodyParams) {
		try {
			ResponseEvent res = componentWrapper.getDsaAdapter().create(bodyParams.getMetadata(),
					bodyParams.getCipheredFilePath(), bodyParams.getTmpSymmetricalKey());
			return new ResponseEntity(res, HttpStatus.OK);
		} catch (IOException e) {
			String message = "Error in calling create on dsaAdapter : " + e.getMessage();
			LOG.severe(message);
		}
		return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@ApiResponses(value = { @ApiResponse(code = 403, message = "Forbidden/Request Unauthorized"),
			@ApiResponse(code = 412, message = "Client Error"),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 200, message = "Request accepted") })
	@ApiOperation(httpMethod = "POST", value = "Receives a DPO from another ISI", tags = {
			"DSA Adapter Front-End API" }, notes = "This method returns a DPO id")
	@RequestMapping(method = { RequestMethod.POST }, value = { "/receiveDPO" }, produces = {
			"application/json" }, consumes = { "multipart/form-data" })
	public ResponseEntity<ResponseEvent> receiveDPO(@RequestPart(value = "input_metadata") String input_metadata,
			@RequestPart(value = "file") MultipartFile file) {
		try {
			ResponseEvent res = componentWrapper.getDsaAdapter().receiveDPO(input_metadata, file);
			return new ResponseEntity(res, HttpStatus.OK);
		} catch (IOException e) {
			String message = "Error in calling create on dsaAdapter : " + e.getMessage();
			LOG.severe(message);
		}
		return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping("/prepareData")
	public ResponseEntity<FileSystemResource> prepareData(@RequestBody String[] dpoIDs,
			@RequestHeader(value = "X-c3isp-input_metadata", defaultValue = "{}") String inputMetadata,
			@RequestParam(value = "serviceName", defaultValue = "read") String serviceName,
			@RequestParam(value = "format") FORMAT format, @RequestParam(value = "data_lake") DATA_LAKE dataLake,
			@RequestParam(value = "type") DATA_TYPE type) {
		String res = null;
		try {
			res = componentWrapper.getDsaAdapter().prepareData(dpoIDs, inputMetadata, serviceName, format, dataLake,
					type);
		} catch (Exception e) {
			String message = "Error in calling prepareData : " + e.getMessage();
			LOG.severe(message);
			new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiResponses(value = { @ApiResponse(code = 403, message = "C3ISP Forbidden/Request Unauthorized"),
			@ApiResponse(code = 412, message = "Client/Parameter Error"),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 200, message = "Request sent") })
	@ApiOperation(httpMethod = "POST", value = "Release vdl related to a sessionId", tags = {
			"DSA Adapter Front-End API" }, notes = "", response = ResponseEvent.class)
	@RequestMapping(method = { RequestMethod.POST }, value = { "/releaseData/{sessionId}" }, produces = {
			"application/json" })
	public ResponseEntity<ResponseEvent> releaseData(@PathVariable(value = "sessionId") String sessionId) {
		ResponseEvent res = null;
		try {
			res = componentWrapper.getDsaAdapter().releaseData(sessionId);
			return new ResponseEntity(res, HttpStatus.OK);
		} catch (DsaAdapterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping("/checkAvailableDpos")
	public ResponseEntity<String[]> checkAvailableDpos(@RequestBody String[] dpoIDs,
			@RequestHeader(value = "X-c3isp-input_metadata", defaultValue = "{}") String inputMetadata,
			@RequestParam(value = "serviceName", defaultValue = "move") String serviceName) {
		String[] dpoList = null;
		try {
			dpoList = componentWrapper.getDsaAdapter().checkAvailableDpos(dpoIDs, inputMetadata, serviceName);
		} catch (DsaAdapterException e) {
			String message = "Error in calling prepareData : " + e.getMessage();
			LOG.severe(message);
			new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity(dpoList, HttpStatus.OK);
	}
}
