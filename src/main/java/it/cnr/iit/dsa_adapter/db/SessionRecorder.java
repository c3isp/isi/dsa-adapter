package it.cnr.iit.dsa_adapter.db;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "session_recorder")
public class SessionRecorder {
	public static final String SESSION_ID_FIELD = "session_id";
	public static final String DATALAKE_URI_FIELD = "datalake_uri";

	@DatabaseField(id = true, columnName = SESSION_ID_FIELD)
	private String session_id;

	@DatabaseField(columnName = DATALAKE_URI_FIELD, canBeNull = false)
	private String datalake_uri;

	public String getSession_id() {
		return session_id;
	}

	public void setSession_id(String session_id) {
		this.session_id = session_id;
	}

	public String getDatalake_uri() {
		return datalake_uri;
	}

	public void setDatalake_uri(String datalake_uri) {
		this.datalake_uri = datalake_uri;
	}

}
