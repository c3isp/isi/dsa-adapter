package it.cnr.iit.dsa_adapter.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import it.cnr.iit.common.reject.Reject;

@Component
public class SessionRecorderStorage {

	private Logger log = Logger.getLogger(SessionRecorderStorage.class.getName());

	private ConnectionSource connection = null;
	private Dao<SessionRecorder, String> dao = null;

	@Value("${db.connection:jdbc:sqlite::memory:}")
	private String dbConnection;

	@PostConstruct
	public void init() {
		refresh();
	}

	private synchronized void refresh() {
		if (connection == null || !connection.isOpen()) {
			try {
				connection = new JdbcPooledConnectionSource(dbConnection);
				dao = DaoManager.createDao(connection, SessionRecorder.class);
				TableUtils.createTableIfNotExists(connection, SessionRecorder.class);
				log.info(() -> this.getClass().getSimpleName() + " database connection established.");
			} catch (SQLException e) {
				log.severe(() -> e.getClass().getSimpleName() + " while refreshing connection : " + e.getMessage());
			}
		}
	}

	public boolean createOrUpdate(SessionRecorder sessionRecorder) {
		try {
			refresh();
			dao.createOrUpdate(sessionRecorder);
			return true;
		} catch (SQLException e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
			return false;
		}
	}

	public boolean delete(SessionRecorder sessionRecorder) {
		try {
			refresh();
			dao.delete(sessionRecorder);
			return true;
		} catch (SQLException e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
			return false;
		}
	}

	public SessionRecorder getForSessionId(String sessionId) {
		return getForField(SessionRecorder.SESSION_ID_FIELD, sessionId);
	}

	private SessionRecorder getForField(String column, String value) {
		List<SessionRecorder> tables = getForFields(column, value);
		if (tables == null || tables.size() > 1) {
			throw new IllegalStateException("Same field used multiple times");
		}
		return tables.stream().findFirst().orElse(null);
	}

	public List<SessionRecorder> getForFields(String column, String value) {
		Reject.ifBlank(column);
		Reject.ifBlank(value);
		try {
			refresh();
			QueryBuilder<SessionRecorder, String> qbAttributes = dao.queryBuilder();
			return qbAttributes.where().eq(column, value).query();
		} catch (Exception e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + column + " : " + value + " , " + e.getMessage());
		}
		return new ArrayList<>();
	}

}
