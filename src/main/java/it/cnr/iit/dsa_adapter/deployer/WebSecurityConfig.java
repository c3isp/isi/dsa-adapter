package it.cnr.iit.dsa_adapter.deployer;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private static final Logger logger = Logger.getLogger(WebSecurityConfig.class.getName());

	public static final String URL_PATH = "/v1/**";

	@Value("${security.activation.status}")
	private boolean securityActivationStatus;

	/**
	 * These paths are required for Swagger and so must not be protected
	 */
	String[] excludeApiPaths = { "/v2/api-docs", "/configuration/ui", "/swagger-resources", "/configuration/security",
			"/swagger-ui.html", "/webjars/**" };

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		logger.log(Level.INFO, "securityActivationStatus={0}", securityActivationStatus);

		if (securityActivationStatus) {
			http.httpBasic();
			http.authorizeRequests().antMatchers(excludeApiPaths).permitAll().antMatchers(URL_PATH).authenticated();
		} else {
			http.authorizeRequests().anyRequest().permitAll();
		}
		http.csrf().disable();
		http.headers().frameOptions().disable();
	}

}
