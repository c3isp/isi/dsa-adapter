package it.cnr.iit.dsa_adapter.deployer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("it.cnr.iit")
public class DsaAdapterFrontendDeployer extends SpringBootServletInitializer {
	public static void main(String[] args) {
		SpringApplication.run(DsaAdapterFrontendDeployer.class, args);
	}
}
