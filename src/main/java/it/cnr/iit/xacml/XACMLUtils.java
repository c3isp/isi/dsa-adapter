package it.cnr.iit.xacml;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.att.research.xacml.api.Attribute;
import com.att.research.xacml.api.Identifier;
import com.att.research.xacml.api.Request;
import com.att.research.xacml.api.RequestAttributes;
import com.att.research.xacml.std.StdMutableRequestAttributes;

public class XACMLUtils {

	private XACMLUtils() {
	}

	public static List<RequestAttributes> mergeRequests(Request partialRequest, Request request) {
		Map<Identifier, StdMutableRequestAttributes> map = partialRequest.getRequestAttributes().stream()
				.collect(Collectors.toMap(RequestAttributes::getCategory, StdMutableRequestAttributes::new));

		for (RequestAttributes attributes : request.getRequestAttributes()) {
			StdMutableRequestAttributes otherAttrs = map.get(attributes.getCategory());
			for (Attribute attr : attributes.getAttributes()) {
				otherAttrs.add(attr);
			}
		}
		return new ArrayList<>(map.values());
	}

}
