package it.cnr.iit.xacml.request_container;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "RequestElement", description = "Holds the request elements")
@XmlRootElement(name = "Request")
public class RequestElement {
	@ApiModelProperty(allowEmptyValue = true, required = true, value = "Attribute")
	List<RequestAttributes> attribute;

	@JsonProperty(value = "Attribute")
	public List<RequestAttributes> getAttributes() {
		return attribute;
	}

	public void setAttributes(List<RequestAttributes> attributes) {
		attribute = attributes;
	}
}
