package it.cnr.iit.xacml.request_container;

import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "BaseCategory", description = "Base class for AccessSubject, Action, Resource, Environment categories")
class BaseCategory {
	@ApiModelProperty(allowEmptyValue = true, required = true, value = "Attribute")
	List<RequestAttributes> attribute = new LinkedList<>();

	@JsonProperty(value = "Attribute")
	public List<RequestAttributes> getAttributes() {
		return attribute;
	}

	public void setAttributes(List<RequestAttributes> attributes) {
		attribute = attributes;
	}
}
