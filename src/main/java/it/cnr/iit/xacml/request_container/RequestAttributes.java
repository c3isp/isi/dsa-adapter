package it.cnr.iit.xacml.request_container;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "RequestAttributes", description = "Container for attributes in a XACML-like fashon")
public class RequestAttributes {
	@XmlElement(name = "AttributeId", nillable = false, required = true)
	@JsonProperty(value = "AttributeId", required = true)
	String attributeId = "";
	@XmlElement(name = "Value", nillable = false, required = true)
	@JsonProperty(value = "Value", required = true)
	String value = "";
	@XmlElement(name = "DataType", nillable = false, required = true)
	@JsonProperty(value = "DataType", required = true)
	String datatype = "string";

	public RequestAttributes() {

	}

	public RequestAttributes(String attributeId, String value, String datatype) {
		this.attributeId = attributeId;
		this.value = value;
		this.datatype = datatype;
	}

	public String getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDatatype() {
		return datatype;
	}

	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RequestAttributes) {
			RequestAttributes input = (RequestAttributes) obj;
			return input.attributeId.equals(attributeId) && input.value.equals(value)
					&& input.datatype.equals(datatype);
		}
		return super.equals(obj);
	}
}
