package it.cnr.iit.xacml.request_container;

import java.io.IOException;
import java.io.StringWriter;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "RequestContainer", description = "Mapping request attributes in a XACML-like fashon")
public class RequestContainer {
	@JsonProperty(value = "Request")
	RequestElement request = new RequestElement();

	@ApiModelProperty(allowEmptyValue = false, required = true, name = "Request")
	public RequestElement getRequest() {
		return request;
	}

	public void setRequest(RequestElement requestElement) {
		request = requestElement;
	}

	public String search(String attributeName) {
		if (request == null) {
			return null;
		}
		if (request.getAttributes().size() < 1) {
			return null;
		}
		for (RequestAttributes attr : request.getAttributes()) {
			if (attr.getAttributeId().compareToIgnoreCase(attributeName) != 0) {
				continue;
			}
			return attr.getValue();
		}
		return null;
	}

	public boolean setAttribute(String attributeName, String value) {
		if (request == null) {
			return false;
		}
		if (request.getAttributes().size() < 1) {
			return false;
		}
		for (RequestAttributes attr : request.getAttributes()) {
			if (attr.getAttributeId().compareToIgnoreCase(attributeName) != 0) {
				continue;
			}
			attr.setValue(value);
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		StringWriter toReturn = new StringWriter();
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(toReturn, this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return toReturn.toString();
	}
}
