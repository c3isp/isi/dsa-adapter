package it.cnr.iit.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.att.research.xacml.api.Request;

import it.cnr.iit.xacml.RequestGenerator;
import it.cnr.iit.xacml.RequestXMLSerializer;

public class MainTests {

	@Test
	public void testXACMLV3RequestGenerator() {
		String user = "Alessandro";

		RequestGenerator requestGenerator = new RequestGenerator();
		Request request = requestGenerator.createXACMLV3Request(user, "dpo1", "read", true);

		RequestXMLSerializer serializer = new RequestXMLSerializer();
		String strRequest = serializer.serialize(request);

		System.out.println(strRequest);
		assertTrue(strRequest.contains(user));
	}
}
